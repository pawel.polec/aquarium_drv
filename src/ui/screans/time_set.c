#include "time_set.h"
#include "ui/view/elements/digit.h"
#include "ui/view/elements/simple_text.h"
#include "ui/view/elements/custom_char.h"
#include "drv/LCD/HD44780.h"
#include "sm/kbd/kbd.h"

#include "ui/screans/manager.h"

#include "framework/db/db.h"

#define SET_TIME_RTC "Ustaw godzin\xea:"
#define SET_TIME_FEED "Pora karmienia:"
#define SET_TIME_LIGHT_ON "Zapal \x9cwiat\xb3o o"
#define SET_TIME_LIGHT_OFF "Zga\x9c \x9cwiat\xb3o o"

static struct _timeMenu {
    __flash const char* title;

    struct UI_digit* H10;
    struct UI_digit* H1;

    struct UI_digit* m10;
    struct UI_digit* m1;

    struct UI_digit* s10;
    struct UI_digit* s1;

    bool screenHasSecs;

    struct tm timeinfo;

    void (*onOk) (enum KbdKey key);

    enum UIScreenMgrScr backTo;
} instance = {0};

static void _refresh(struct UI_digit *d1, struct UI_digit *d10, uint8_t value) {
    UI_digitSetValue(d1, value%10);
    UI_digitSetValue(d10, value/10);
}

static int8_t _validate60(int8_t val) {
    return ((val>=0) ? val%60 : 60+val);
}

static void _onIncH() {
    ++instance.timeinfo.tm_hour;
    instance.timeinfo.tm_hour%=24;
    _refresh(instance.H1, instance.H10, instance.timeinfo.tm_hour);
}

static void _onDecH() {
    instance.timeinfo.tm_hour = (instance.timeinfo.tm_hour <= 0) ? 23 : instance.timeinfo.tm_hour-1;
    _refresh(instance.H1, instance.H10, instance.timeinfo.tm_hour);
}


static void _onInc10H() {
    instance.timeinfo.tm_hour+=10;
    if (instance.timeinfo.tm_hour>23) instance.timeinfo.tm_hour-=20;
    _refresh(instance.H1, instance.H10, instance.timeinfo.tm_hour);
}

static void _onDec10H() {
    instance.timeinfo.tm_hour = (instance.timeinfo.tm_hour < 10) ? (20 + instance.timeinfo.tm_hour%10) : instance.timeinfo.tm_hour-10;
    if (instance.timeinfo.tm_hour>23) instance.timeinfo.tm_hour-=10;
    _refresh(instance.H1, instance.H10, instance.timeinfo.tm_hour);
}

static void _onIncm() {
    instance.timeinfo.tm_min = _validate60(instance.timeinfo.tm_min+1);
    _refresh(instance.m1, instance.m10, instance.timeinfo.tm_min);
}

static void _onDecm() {
    instance.timeinfo.tm_min = _validate60(instance.timeinfo.tm_min-1);
    _refresh(instance.m1, instance.m10, instance.timeinfo.tm_min);
}


static void _onInc10m() {
    instance.timeinfo.tm_min = _validate60(instance.timeinfo.tm_min+10);
    _refresh(instance.m1, instance.m10, instance.timeinfo.tm_min);
}

static void _onDec10m() {
    instance.timeinfo.tm_min = _validate60(instance.timeinfo.tm_min-10);
    _refresh(instance.m1, instance.m10, instance.timeinfo.tm_min);
}

static void _onIncs() {
    instance.timeinfo.tm_sec = _validate60(instance.timeinfo.tm_sec+1);
    _refresh(instance.s1, instance.s10, instance.timeinfo.tm_sec);
}

static void _onDecs() {
    instance.timeinfo.tm_sec = _validate60(instance.timeinfo.tm_sec-1);
    _refresh(instance.s1, instance.s10, instance.timeinfo.tm_sec);
}


static void _onInc10s() {
    instance.timeinfo.tm_sec = _validate60(instance.timeinfo.tm_sec+10);
    _refresh(instance.s1, instance.s10, instance.timeinfo.tm_sec);
}

static void _onDec10s() {
    instance.timeinfo.tm_sec = _validate60(instance.timeinfo.tm_sec-10);
    _refresh(instance.s1, instance.s10, instance.timeinfo.tm_sec);
}


static void _onKeyBack(enum KbdKey key) {
    UI_ScreenMgrSwitch(instance.backTo);
}

static void _onRTCKeyOK(enum KbdKey key) {
    RTC_SetTime(&instance.timeinfo);
    RTC_StoreTime ();
    UI_ScreenMgrSwitch(instance.backTo);
}

static void _onFeedKeyOK(enum KbdKey key) {
    DBInstance *db;
    DB_GetDB(&db);
	time_t tmp = mktime(&instance.timeinfo);

    DB_PutEntry(db, DB_FIELD_ID_FEED_TIME, (uint8_t*) &tmp, sizeof(tmp));
    UI_ScreenMgrSwitch(instance.backTo);
}

static void _onLightOnKeyOK(enum KbdKey key) {
    DBInstance *db;
    DB_GetDB(&db);
	time_t tmp = mktime(&instance.timeinfo);

    DB_PutEntry(db, DB_FIELD_ID_LIGHT_ON, (uint8_t*) &tmp, sizeof(tmp));
    UI_ScreenMgrSwitch(instance.backTo);
}

static void _onLightOffKeyOK(enum KbdKey key) {
    DBInstance *db;
    DB_GetDB(&db);
	time_t tmp = mktime(&instance.timeinfo);

    DB_PutEntry(db, DB_FIELD_ID_LIGHT_OFF, (uint8_t*) &tmp, sizeof(tmp));
    UI_ScreenMgrSwitch(instance.backTo);
}



static Error _setTimeMenuInit() {

    struct ui_pos pos = {.x = 1, .y=0};

    LCD_Clear();

    {
        enum CustomChar charset[CUSTOMCHAR_SPACE]  =  {
            CUSTOM_CHAR_BUTTON_UP,
            CUSTOM_CHAR_BUTTON_DOWN,
            CUSTOM_CHAR_BUTTON_LEFT,
            CUSTOM_CHAR_BUTTON_RIGHT,
            CUSTOM_CHAR_E_WITH_OGONEK,
            CUSTOM_CHAR_N_WITH_ACUTE_ACCENT,
            CUSTOM_CHAR_S_WITH_ACUTE_ACCENT,
            CUSTOM_CHAR_L_WITH_STROKE
        };

        UI_customCharSetCharset(charset);
    }

    UI_SimpleText(&pos, 0, instance.title);

    UI_digitInit();
    
    pos.x=1; pos.y=2;
    UI_SimpleText(&pos, 1, PSTR("\2"));
    pos.x=2;
    instance.H10 = UI_digitGet(&pos, instance.timeinfo.tm_hour/10, _onInc10H, _onDec10H);
    pos.x=3;
    instance.H1 = UI_digitGet(&pos, instance.timeinfo.tm_hour%10, _onIncH, _onDecH);
    UI_digitTanglePrev(instance.H1, instance.H10);

    pos.x=4;
    UI_SimpleText(&pos, 1, PSTR(":"));

    pos.x=5;
    instance.m10 = UI_digitGet(&pos, (instance.timeinfo.tm_min)/10, _onInc10m, _onDec10m);
    UI_digitTanglePrev(instance.m10, instance.H1);
    pos.x=6;
    instance.m1 = UI_digitGet(&pos, (instance.timeinfo.tm_min)%10, _onIncm, _onDecm);
    UI_digitTanglePrev(instance.m1, instance.m10);

    if (instance.screenHasSecs) {
        pos.x=7;
        UI_SimpleText(&pos, 1, PSTR(":"));

        pos.x=8;
        instance.s10 = UI_digitGet(&pos, (instance.timeinfo.tm_sec%100)/10, _onInc10s, _onDec10s);
        UI_digitTanglePrev(instance.s10, instance.m1);
        pos.x=9;
        instance.s1 = UI_digitGet(&pos, instance.timeinfo.tm_sec%10, _onIncs, _onDecs);
        pos.x=10;
        UI_SimpleText(&pos, 1, PSTR("\3"));
        UI_digitTanglePrev(instance.s1, instance.s10);
        UI_digitTanglePrev(instance.H10, instance.s1);
    } else {
        pos.x=7;
        UI_SimpleText(&pos, 1, PSTR("\3"));
        UI_digitTanglePrev(instance.H10, instance.m1);
    }
    UI_digitSetActive(instance.H1);

    SM_KbdSetCbfs(KBD_KEY_CANCEL, _onKeyBack);
    SM_KbdSetCbfs(KBD_KEY_ENTER, instance.onOk);

    return NO_ERROR;
}

Error _setTimeMenuRun() {
    return NO_ERROR;
}

Error _setTimeMenuExit() {
    SM_KbdClearCbfs();
    UI_digitFreeAll ();
    return NO_ERROR;
}


static Error _setRTCTimeMenuInit() {
    RTC_GetTime(&instance.timeinfo);

    instance.title = PSTR(SET_TIME_RTC);
    instance.onOk = _onRTCKeyOK;
    instance.screenHasSecs = true;
    instance.backTo = UI_SCR_MGR_DATE_MENU;
    return _setTimeMenuInit();
}

static Error _setFeedTimeMenuInit() {
    DBInstance *db;
    DB_GetDB(&db);
    time_t timestamp;

    if (ERROR_NOT_FOUND ==  DB_GetEntry(db, DB_FIELD_ID_FEED_TIME, (uint8_t*) &timestamp, sizeof(timestamp))) {
        timestamp = 0;
    }

    gmtime_r(&timestamp, &instance.timeinfo);

    instance.title = PSTR(SET_TIME_FEED);
    instance.onOk = _onFeedKeyOK;
    instance.screenHasSecs = false;
    instance.backTo = UI_SCR_MGR_AQUARUM_MENU;
    return _setTimeMenuInit();
}

static Error _setLightOnTimeMenuInit() {
    DBInstance *db;
    DB_GetDB(&db);
    time_t timestamp;

    if (ERROR_NOT_FOUND ==  DB_GetEntry(db, DB_FIELD_ID_LIGHT_ON, (uint8_t*) &timestamp, sizeof(timestamp))) {
        timestamp = 0;
    }

    gmtime_r(&timestamp, &instance.timeinfo);

    instance.title = PSTR(SET_TIME_LIGHT_ON);
    instance.onOk = _onLightOnKeyOK;
    instance.screenHasSecs = false;
    instance.backTo = UI_SCR_MGR_AQUARUM_MENU;
    return _setTimeMenuInit();
}

static Error _setLightOffTimeMenuInit() {
    DBInstance *db;
    DB_GetDB(&db);
    time_t timestamp;

    if (ERROR_NOT_FOUND ==  DB_GetEntry(db, DB_FIELD_ID_LIGHT_OFF, (uint8_t*) &timestamp, sizeof(timestamp))) {
        timestamp = 0;
    }

    gmtime_r(&timestamp, &instance.timeinfo);

    instance.title = PSTR(SET_TIME_LIGHT_OFF);
    instance.onOk = _onLightOffKeyOK;
    instance.screenHasSecs = false;
    instance.backTo = UI_SCR_MGR_AQUARUM_MENU;
    return _setTimeMenuInit();
}

Error UI_ScreenMenuSetTimeRegisterScreen() {
    UI_ScreenMgrRegisterScreen(UI_SCR_MGR_SET_RTC_TIME_MENU, _setRTCTimeMenuInit, _setTimeMenuRun, _setTimeMenuExit);
    UI_ScreenMgrRegisterScreen(UI_SCR_MGR_SET_FEED_TIME_MENU, _setFeedTimeMenuInit, _setTimeMenuRun, _setTimeMenuExit);
    UI_ScreenMgrRegisterScreen(UI_SCR_MGR_SET_LIGHT_ON_TIME_MENU, _setLightOnTimeMenuInit, _setTimeMenuRun, _setTimeMenuExit);
    UI_ScreenMgrRegisterScreen(UI_SCR_MGR_SET_LIGHT_OFF_TIME_MENU, _setLightOffTimeMenuInit, _setTimeMenuRun, _setTimeMenuExit);

    return NO_ERROR;
}