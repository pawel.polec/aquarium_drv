#include "date_set.h"
#include "ui/view/elements/digit.h"
#include "ui/view/elements/simple_text.h"
#include "ui/view/elements/custom_char.h"
#include "sm/kbd/kbd.h"
#include "ui/screans/manager.h"
#include "drv/LCD/HD44780.h"
#include "drv/rtc/rtc.h"
#include <avr/pgmspace.h>

#define SET_DATE_TITLE ("Ustaw dat\xea")

static struct _dateMenu {
    struct UI_digit* Y10;
    struct UI_digit* Y1;

    struct UI_digit* M10;
    struct UI_digit* M1;

    struct UI_digit* D10;
    struct UI_digit* D1;

    uint8_t day;
    uint8_t month;
    uint8_t year;

    struct tm timeinfo;
} instance = {0};

static void _refreshDay() {
    UI_digitSetValue(instance.D1, instance.timeinfo.tm_mday%10);
    UI_digitSetValue(instance.D10, instance.timeinfo.tm_mday/10);
}

static void _refreshMonth() {
    UI_digitSetValue(instance.M1, (instance.timeinfo.tm_mon+1)%10);
    UI_digitSetValue(instance.M10, (instance.timeinfo.tm_mon+1)/10);
}

static void _refreshYear() {
    UI_digitSetValue(instance.Y1, instance.timeinfo.tm_year%10);
    UI_digitSetValue(instance.Y10, (instance.timeinfo.tm_year%100)/10);
}

static void _onIncDay() {
    instance.timeinfo.tm_mday = (instance.timeinfo.tm_mday+1)%month_length(instance.timeinfo.tm_year, instance.timeinfo.tm_mon+1 );
    _refreshDay();
}

static void _onDecDay() {
    instance.timeinfo.tm_mday =  (instance.timeinfo.tm_mday <= 1 ) ? month_length(instance.timeinfo.tm_year, instance.timeinfo.tm_mon+1 ) : instance.timeinfo.tm_mday-1;
    _refreshDay();
}

static void _onInc10Days() {
    instance.timeinfo.tm_mday = (instance.timeinfo.tm_mday+10 > month_length(instance.timeinfo.tm_year, instance.timeinfo.tm_mon+1 ) ) ? instance.timeinfo.tm_mday%10 : instance.timeinfo.tm_mday + 10;
    if (!instance.timeinfo.tm_mday) instance.timeinfo.tm_mday=10;
    _refreshDay();
}

static void _onDec10Days() {
    instance.timeinfo.tm_mday = (instance.timeinfo.tm_mday <= 10 ) ? (month_length(instance.timeinfo.tm_year, instance.timeinfo.tm_mon+1 )/10)*10+instance.timeinfo.tm_mday%10 : instance.timeinfo.tm_mday-10;
    _refreshDay();
}

static void _onIncMonth() {
    instance.timeinfo.tm_mon=(instance.timeinfo.tm_mon+1)%12;
    _refreshMonth();
}

static void _onDecMonth() {
    instance.timeinfo.tm_mon = (instance.timeinfo.tm_mon == 0) ? 11 : instance.timeinfo.tm_mon-1;
    _refreshMonth();
}

static void _onIncYear() {
    ++instance.timeinfo.tm_year;
    _refreshYear();
}

static void _onDecYear() {
    --instance.timeinfo.tm_year;
    _refreshYear();
}

static void _onInc10Years() {
    instance.timeinfo.tm_year+=10;
    _refreshYear();
}

static void _onDec10Years() {
    instance.timeinfo.tm_year-=10;
    _refreshYear();
}

static void _onKeyBack(enum KbdKey key) {
    UI_ScreenMgrSwitch(UI_SCR_MGR_SET_DATE_MENU);
}

static void _onKeyOK(enum KbdKey key) {
    uint8_t ml = month_length(instance.timeinfo.tm_year, instance.timeinfo.tm_mon+1 );
    if (instance.timeinfo.tm_mday > ml) {
        instance.timeinfo.tm_mday = ml;
        _refreshDay();
    } else {
        RTC_SetTime(&instance.timeinfo);
        RTC_StoreTime ();
        UI_ScreenMgrSwitch(UI_SCR_MGR_SET_DATE_MENU);
    }

}

static Error _setDateMenuInit() {

    struct ui_pos pos = {.x = 1, .y=0};

    LCD_Clear();

    {
        enum CustomChar charset[CUSTOMCHAR_SPACE]  =  {
            CUSTOM_CHAR_BUTTON_UP,
            CUSTOM_CHAR_BUTTON_DOWN,
            CUSTOM_CHAR_BUTTON_LEFT,
            CUSTOM_CHAR_BUTTON_RIGHT,
            CUSTOM_CHAR_E_WITH_OGONEK,
            CUSTOM_CHAR_NULL,
            CUSTOM_CHAR_NULL,
            CUSTOM_CHAR_NULL
        };

        UI_customCharSetCharset(charset);
    }

    RTC_GetTime(&instance.timeinfo);

    UI_SimpleText(&pos, sizeof(SET_DATE_TITLE), PSTR(SET_DATE_TITLE));

    UI_digitInit();
    
    pos.x=1; pos.y=2;
    UI_SimpleText(&pos, 1, PSTR("\2"));
    pos.x=2;
    instance.D10 = UI_digitGet(&pos, instance.timeinfo.tm_mday/10, _onInc10Days, _onDec10Days);
    pos.x=3;
    instance.D1 = UI_digitGet(&pos, instance.timeinfo.tm_mday%10, _onIncDay, _onDecDay);
    UI_digitTanglePrev(instance.D1, instance.D10);

    pos.x=4;
    UI_SimpleText(&pos, 1, PSTR("."));

    pos.x=5;
    instance.M10 = UI_digitGet(&pos, (instance.timeinfo.tm_mon+1)/10, NULL, NULL);
    pos.x=6;
    instance.M1 = UI_digitGet(&pos, (instance.timeinfo.tm_mon+1)%10, _onIncMonth, _onDecMonth);
    UI_digitTanglePrev(instance.M1, instance.D1);

    pos.x=7;
    UI_SimpleText(&pos, 3, PSTR(".20"));

    pos.x=10;
    instance.Y10 = UI_digitGet(&pos, (instance.timeinfo.tm_year%100)/10, _onInc10Years, _onDec10Years);
    UI_digitTanglePrev(instance.Y10, instance.M1);
    pos.x=11;
    instance.Y1 = UI_digitGet(&pos, instance.timeinfo.tm_year%10, _onIncYear, _onDecYear);
    pos.x=12;
    UI_SimpleText(&pos, 1, PSTR("\3"));
    UI_digitTanglePrev(instance.Y1, instance.Y10);
    UI_digitTanglePrev(instance.D10, instance.Y1);
    UI_digitSetActive(instance.D1);

    SM_KbdSetCbfs(KBD_KEY_CANCEL, _onKeyBack);
    SM_KbdSetCbfs(KBD_KEY_ENTER, _onKeyOK);

    return NO_ERROR;
}

Error _setDateMenuRun() {
    return NO_ERROR;
}

Error _setDateMenuExit() {
    SM_KbdClearCbfs();
    UI_digitFreeAll ();
    return NO_ERROR;
}

Error UI_ScreenMenuSetDateRegisterScreen() {

    UI_ScreenMgrRegisterScreen(UI_SCR_MGR_SET_DATE_MENU, _setDateMenuInit, _setDateMenuRun, _setDateMenuExit);

    return NO_ERROR;
}


