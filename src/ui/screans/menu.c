#include "menu.h"
#include "manager.h"

#include "drv/LCD/HD44780.h"
#include "sm/kbd/kbd.h"
#include "ui/screans/manager.h"
#include "ui/view/elements/custom_char.h"
#include "ui/view/elements/scroll_bar.h"

#include <avr/pgmspace.h>
#include <string.h>

struct ui_menu_pair {
    __flash const char* label;
    enum UIScreenMgrScr detination;
};

__flash const char * __flash const CleanLine = (__flash const char[]) {"                   "};
__flash const char * __flash const DashedLine = (__flash const char[]) {"-------------------+"};

struct ui_menu {
    __flash const char * title;
    __flash const struct ui_menu_pair* list;
    uint8_t elements;
    enum UIScreenMgrScr onBackDetination;
};

/////////////////////////////////////
// MAIN MENU
/////////////////////////////////////

__flash const struct ui_menu_pair ui_main_menu_options[] = {
    {
        .label = (__flash const char[]) {"Akwarium"},
        .detination = UI_SCR_MGR_AQUARUM_MENU,
    },
    {
        .label = (__flash const char[]) {"Wy\x9Cwietlacz"},
        .detination = UI_SCR_MGR_NULL,
    },
    {
        .label = (__flash const char[]) {"Data"},
        .detination = UI_SCR_MGR_DATE_MENU,
    },
    {
        .label = (__flash const char[]) {"Reset"},
        .detination = UI_SCR_MGR_NULL,
    },
};

__flash const struct ui_menu ui_main_menu = {
    .title = (__flash const char[]) {"MENU"},
    .list = ui_main_menu_options,
    .elements = sizeof(ui_main_menu_options)/sizeof(ui_main_menu_options[0]),
    .onBackDetination = UI_SCR_MGR_MAIN
};

/////////////////////////////////////
// AQUARUIM MENU
/////////////////////////////////////

__flash const struct ui_menu_pair ui_aquarium_menu_options[] = {
    {
        .label = (__flash const char[]) {"Termostat"},
        .detination = UI_SCR_MGR_TEMPERATURE_MENU,
    },
    {
        .label = (__flash const char[]) {"Pora karmienia"},
        .detination = UI_SCR_MGR_SET_FEED_TIME_MENU,
    },
    {
        .label = (__flash const char[]) {"Pora o\x9Cwietlenia"},
        .detination = UI_SCR_MGR_SET_LIGHT_ON_TIME_MENU,
    },
    {
        .label = (__flash const char[]) {"Pora gaszenia"},
        .detination = UI_SCR_MGR_SET_LIGHT_OFF_TIME_MENU,
    },
    {
        .label = (__flash const char[]) {"Konserwacja"},
        .detination = UI_SCR_MGR_NULL,
    },
};

__flash const struct ui_menu ui_aquarium_menu = {
    .title = (__flash const char[]) {"AKWARIUM"},
    .list = ui_aquarium_menu_options,
    .elements = sizeof(ui_aquarium_menu_options)/sizeof(ui_aquarium_menu_options[0]),
    .onBackDetination = UI_SCR_MGR_MAIN_MENU
};

/////////////////////////////////////
// TEMPERATURE MENU
/////////////////////////////////////

__flash const struct ui_menu_pair ui_temperature_menu_options[] = {
    {
        .label = (__flash const char[]) {"Temperatura"},
        .detination = UI_SCR_MGR_NULL,
    },
    {
        .label = (__flash const char[]) {"Histereza"},
        .detination = UI_SCR_MGR_NULL,
    },
};

__flash const struct ui_menu ui_temperature_menu = {
    .title = (__flash const char[]) {"TERMOSTAT"},
    .list = ui_temperature_menu_options,
    .elements = sizeof(ui_temperature_menu_options)/sizeof(ui_temperature_menu_options[0]),
    .onBackDetination = UI_SCR_MGR_AQUARUM_MENU
};

/////////////////////////////////////
// DATE MENU
/////////////////////////////////////

__flash const struct ui_menu_pair ui_date_menu_options[] = {
    {
        .label = (__flash const char[]) {"Dzie\xf1"},
        .detination = UI_SCR_MGR_SET_DATE_MENU,
    },
    {
        .label = (__flash const char[]) {"Godzina"},
        .detination = UI_SCR_MGR_SET_RTC_TIME_MENU,
    },
};

__flash const struct ui_menu ui_date_menu = {
    .title = (__flash const char[]) {"DATA"},
    .list = ui_date_menu_options,
    .elements = sizeof(ui_date_menu_options)/sizeof(ui_date_menu_options[0]),
    .onBackDetination = UI_SCR_MGR_MAIN_MENU
};

struct _ui_menu { //TODO: rename it
    __flash const struct ui_menu* menu;
    uint8_t currentOption;
} instance = {0};

static void _drawMenu(uint8_t start, uint8_t end, uint8_t indicatorPos) {
    uint8_t i;
    for (i=0; i<MIN(end-start, 3); ++i) {
        LCD_GoTo(0,i+1);
        LCD_WriteData((indicatorPos == i) ? 201 : ' ' );
        LCD_WriteTextP(CleanLine);
        LCD_GoTo(2,i+1);
        LCD_WriteTextP(instance.menu->list[i+start].label);
    }
}

static void _redrawMenu() {
    if (instance.currentOption == 0) {//first element focused
        _drawMenu(0, instance.menu->elements, 0);
        if ( instance.menu->elements>3 ) UI_ScrollBarSetValue(0);
    } else if (instance.currentOption == instance.menu->elements-1) { //last element focused
        uint8_t startFrom = (instance.menu->elements<3) ? 0 : instance.menu->elements-3;
        uint8_t indPos = (instance.menu->elements<3) ? instance.menu->elements-1 : 2;
        _drawMenu(startFrom, instance.menu->elements, indPos);
        if ( instance.menu->elements>3 ) UI_ScrollBarSetValue(instance.currentOption-2);
    } else {
        _drawMenu(instance.currentOption-1, instance.menu->elements, 1);
        if ( instance.menu->elements>3 ) UI_ScrollBarSetValue(instance.currentOption-1);
    }
}

static void _redrawScreen() {
    LCD_Clear();
    LCD_GoTo(0,0);
    LCD_WriteTextP(DashedLine);
    LCD_GoTo(1,0);
    LCD_WriteTextP(instance.menu->title);
    _redrawMenu();
}

static void _onKeyOK(enum KbdKey key) {
    UI_ScreenMgrSwitch(instance.menu->list[instance.currentOption].detination);
}

static void _onKeyBack(enum KbdKey key) {
    UI_ScreenMgrSwitch(instance.menu->onBackDetination);
}

static void _onKeyUp(enum KbdKey key) {
    if (instance.currentOption) {
        --instance.currentOption;
    } else {
        instance.currentOption = instance.menu->elements-1;
    }
    _redrawMenu();
}

static void _onKeyDown(enum KbdKey key) {
    instance.currentOption = (instance.currentOption+1)%instance.menu->elements;
    _redrawMenu();
}

Error _menuRun() {
    return NO_ERROR;
}

static Error _menuCommonInit() {
    {
        enum CustomChar charset[CUSTOMCHAR_SPACE]  =  {
            CUSTOM_CHAR_N_WITH_ACUTE_ACCENT,
            CUSTOM_CHAR_SUN,
            CUSTOM_CHAR_MOON,
            CUSTOM_CHAR_DROP,
            CUSTOM_CHAR_THERMO,
            CUSTOM_CHAR_EATMAN,
            CUSTOM_CHAR_CELS_DEG,
            CUSTOM_CHAR_S_WITH_ACUTE_ACCENT
        };
        
        UI_customCharSetCharset(charset); 
    }

    {
        SM_KbdSetCbfs(KBD_KEY_ENTER, _onKeyOK);
        SM_KbdSetCbfs(KBD_KEY_CANCEL, _onKeyBack);
        SM_KbdSetCbfs(KBD_KEY_UP, _onKeyUp);
        SM_KbdSetCbfs(KBD_KEY_DOWN, _onKeyDown);
    }
    
    _redrawScreen();
    if (instance.menu->elements>3) {
        UI_ScrollBarInit((struct ui_pos[] ) {{.x=19, .y=1}}, 3, instance.menu->elements-3, 0);
    }

    return NO_ERROR;
}

static Error _mainMenuInit() {

    instance.menu = &ui_main_menu;
    instance.currentOption = 0;

    //LCD_WriteData(201);

    _menuCommonInit();

    return NO_ERROR;
}

static Error _aquariumMenuInit() {
    instance.menu = &ui_aquarium_menu;
    instance.currentOption = 0;

    _menuCommonInit();

    return NO_ERROR;
}

static Error _temperatureMenuInit() {
    instance.menu = &ui_temperature_menu;
    instance.currentOption = 0; //TODO: (calculate based on prev one?)

    _menuCommonInit();

    return NO_ERROR;
}

static Error _dateMenuInit() {
    instance.menu = &ui_date_menu;
    instance.currentOption = 0; //TODO: (calculate based on prev one?)

    _menuCommonInit();

    return NO_ERROR;
}

Error _menuExit() {
    SM_KbdClearCbfs();
    return NO_ERROR;
}

Error UI_ScreenMenuRegisterScreen() {
    UI_ScreenMgrRegisterScreen(UI_SCR_MGR_MAIN_MENU, _mainMenuInit, _menuRun, _menuExit);
    UI_ScreenMgrRegisterScreen(UI_SCR_MGR_AQUARUM_MENU, _aquariumMenuInit, _menuRun, _menuExit);
    UI_ScreenMgrRegisterScreen(UI_SCR_MGR_TEMPERATURE_MENU, _temperatureMenuInit, _menuRun, _menuExit);
    UI_ScreenMgrRegisterScreen(UI_SCR_MGR_DATE_MENU, _dateMenuInit, _menuRun, _menuExit);
    return NO_ERROR;
}
