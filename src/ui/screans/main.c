#include "main.h"
#include "manager.h"
#include "avr/pgmspace.h"
#include "../view/elements/label.h"
#include "../view/elements/simple_text.h"
#include "drv/rtc/rtc.h"
#include "sm/sensor/temperature.h"
#include "sm/control/control.h"
#include "sm/kbd/kbd.h"
#include "../view/elements/custom_char.h"
#include "drv/peripheral/peripherials.h"

#include "drv/LCD/HD44780.h"

#define UI_MAIN_DATE_TEXT_SIZE (11)
#define UI_MAIN_TIME_TEXT_SIZE (9)
#define UI_MAIN_TEMP_MEAS_SIZE (10)
#define UI_MAIN_HEATER_STATE_SIZE (6)

static void _onKeyMenu(enum KbdKey key) {
    UI_ScreenMgrSwitch(UI_SCR_MGR_MAIN_MENU);
}

static void _onKeyLight(enum KbdKey key) {
    static bool on = true;
    Peripherials_Set(PERIPH_DAYLIGHT, (on) ? PERIPH_STATE_ON : PERIPH_STATE_OFF);
    on = !on;
}

struct ui_mainScreen {
    struct ui_label* labelDate;
    char dateTxt[UI_MAIN_DATE_TEXT_SIZE];
    struct ui_label* labelTime;
    char timeTxt[UI_MAIN_TIME_TEXT_SIZE];
    struct ui_label* labelLightOn;
    char lightsOnTxt[6];
    struct ui_label* labelLightOff;
    char lightsOffTxt[6];
    struct ui_label* labelTMeasur;
    char TMeasurTxt[UI_MAIN_TEMP_MEAS_SIZE];
    struct ui_label* labelService;
    char serviceTxt[6];
    struct ui_label* labelFeeding;
    char feedingTxt[6];
    struct ui_label* labelTSet;
    char TSetTxt[6];
    struct ui_label* labelHeaterState;
    char HeaterStateTxt[UI_MAIN_HEATER_STATE_SIZE];

    bool dateNeedsUpdate:1;
};

static struct ui_mainScreen mainScreen = {0}; 

static void _RtcCbf() {
    mainScreen.dateNeedsUpdate = true;
}

static void _HeaterStateCbf(enum sm_heater_state state) {
    snprintf_P(mainScreen.HeaterStateTxt, UI_MAIN_HEATER_STATE_SIZE, PSTR("\7 %s"), (state == SM_HEATER_ON) ? "ON" : "OFF");
    UI_RefreshLabel(mainScreen.labelHeaterState);
}

static void _TempCbf(uint8_t index) {
    uint16_t t = SM_TempGet(index);
    
    snprintf_P(mainScreen.TMeasurTxt, UI_MAIN_TEMP_MEAS_SIZE, PSTR("\4%d:%2d,%02d\6"), index+1 , t/100, t%100);
    UI_RefreshLabel(mainScreen.labelTMeasur);
}

Error UI_ScreenMainExit() {
    SM_KbdClearCbfs();
    RTC_UnegisterOnSecCbf(_RtcCbf);
    RTC_UnregisterOnTimeChangeCbf(_RtcCbf);
    SM_TempUnregister(_TempCbf);
    SM_CtrlTempHeaterUnregister(_HeaterStateCbf);

    UI_FreeLabel(mainScreen.labelDate);
    UI_FreeLabel(mainScreen.labelTime);
    UI_FreeLabel(mainScreen.labelTMeasur);
    UI_FreeLabel(mainScreen.labelHeaterState);

    return NO_ERROR;
}


Error UI_ScreenMainRun() {
    if (mainScreen.dateNeedsUpdate) {
        struct tm time;
        RTC_GetTime(&time);
        snprintf_P(mainScreen.dateTxt, UI_MAIN_DATE_TEXT_SIZE, PSTR("%S%02d.%02d.%02d"), RTC_GetDayName(RTC_GetDayOfWeek()), time.tm_mday, time.tm_mon+1, (time.tm_year+RTC_BASE_YEAR)%100);
        UI_RefreshLabel(mainScreen.labelDate);
        snprintf_P(mainScreen.timeTxt, UI_MAIN_TIME_TEXT_SIZE, PSTR("%02d:%02d:%02d"), time.tm_hour, time.tm_min, time.tm_sec);
        UI_RefreshLabel(mainScreen.labelTime);
        mainScreen.dateNeedsUpdate=false;
    }
    return NO_ERROR;
}

Error UI_ScreenMainRegisterScreen() {
    return UI_ScreenMgrRegisterScreen(UI_SCR_MGR_MAIN, UI_ScreenMainInit, UI_ScreenMainRun, UI_ScreenMainExit);
}

Error UI_ScreenMainInit() {
    {
        enum CustomChar charset[CUSTOMCHAR_SPACE]  =  {
            CUSTOM_CHAR_CAP_S_WITH_ACUTE_ACCENT,
            CUSTOM_CHAR_SUN,
            CUSTOM_CHAR_MOON,
            CUSTOM_CHAR_DROP,
            CUSTOM_CHAR_THERMO,
            CUSTOM_CHAR_EATMAN,
            CUSTOM_CHAR_CELS_DEG,
            CUSTOM_CHAR_FIRE
        };

        UI_customCharSetCharset(charset);
    }

    {
        SM_KbdSetCbfs(KBD_KEY_MENU, _onKeyMenu);
        SM_KbdSetCbfs(KBD_KEY_DAYLIGHT, _onKeyLight);
    }

    LCD_Clear();

	{
        struct ui_pos label_pos;

        label_pos.x = 0; label_pos.y = 0;
        mainScreen.labelDate = UI_GetLabel(&label_pos, UI_MAIN_DATE_TEXT_SIZE-1, mainScreen.dateTxt, sizeof(mainScreen.dateTxt));
        UI_RefreshLabel(mainScreen.labelDate);

        label_pos.x = 1; label_pos.y = 1;
        mainScreen.labelTime = UI_GetLabel(&label_pos, UI_MAIN_TIME_TEXT_SIZE-1, mainScreen.timeTxt, sizeof(mainScreen.timeTxt));
        UI_RefreshLabel(mainScreen.labelTime);

        label_pos.x = 11; label_pos.y = 1;
        mainScreen.labelTMeasur = UI_GetLabel(&label_pos, UI_MAIN_TEMP_MEAS_SIZE-1, mainScreen.TMeasurTxt, sizeof(mainScreen.TMeasurTxt));
        UI_RefreshLabel(mainScreen.labelTMeasur);

        label_pos.x = 14; label_pos.y = 2;
        mainScreen.labelHeaterState = UI_GetLabel(&label_pos, UI_MAIN_HEATER_STATE_SIZE-1, mainScreen.HeaterStateTxt, sizeof(mainScreen.HeaterStateTxt));
        UI_RefreshLabel(mainScreen.labelHeaterState);

        label_pos.x = 12; label_pos.y = 0;
        UI_SimpleText(&label_pos, sizeof("Odczyty"), PSTR("Odczyty"));
    }
    //RTC
    RTC_RegisterOnSecCbf(_RtcCbf);
    RTC_RegisterOnTimeChangeCbf(_RtcCbf);
    SM_TempRegister(_TempCbf);
    SM_CtrlTempHeaterRegister(_HeaterStateCbf);

    mainScreen.dateNeedsUpdate = true;
    return NO_ERROR;
}
