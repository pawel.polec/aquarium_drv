#include "manager.h"

static struct screenMgr {
    struct _screens {
        _screenInit initCbf;
        _screenRun runCbf;
        _screenExit exitCbf;
    } screens[UI_SCR_MGR_COUNT];
    enum UIScreenMgrScr currentScreen;
} instance = {0};

Error UI_ScreenMgrRun() {
    if (instance.currentScreen < UI_SCR_MGR_COUNT && instance.screens[instance.currentScreen].runCbf) {
        return instance.screens[instance.currentScreen].runCbf();
    }
    return ERROR_INVALID_OPERATION;
}

Error UI_ScreenMgrInit() {
    instance.currentScreen = UI_SCR_MGR_NULL;
    return NO_ERROR;
}

Error UI_ScreenMgrRegisterScreen(enum UIScreenMgrScr screen, _screenInit initCbf, _screenRun runCbf, _screenExit exitCbf) {
    if (screen >= UI_SCR_MGR_COUNT) return ERROR_INVALID_PARAMETER;

    instance.screens[screen].initCbf = initCbf;
    instance.screens[screen].runCbf = runCbf;
    instance.screens[screen].exitCbf = exitCbf;

    return NO_ERROR;
}

Error UI_ScreenMgrSwitch(enum UIScreenMgrScr screen) {
    if (screen >= UI_SCR_MGR_COUNT) return ERROR_INVALID_PARAMETER;

    if (screen != instance.currentScreen) {
        if (instance.currentScreen < UI_SCR_MGR_COUNT && instance.screens[instance.currentScreen].exitCbf) instance.screens[instance.currentScreen].exitCbf();
        instance.currentScreen = screen;
        return (instance.screens[instance.currentScreen].exitCbf) ? instance.screens[instance.currentScreen].initCbf() : ERROR_INVALID_OPERATION; //TODO: Display FATAL ERROR on error
    }

    return NO_ERROR;
}