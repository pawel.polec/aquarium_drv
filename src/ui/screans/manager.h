#ifndef UI_SCREEN_MGR_H_
#define UI_SCREEN_MGR_H_

#include "framework/error.h"

enum UIScreenMgrScr {
    UI_SCR_MGR_MAIN,
    UI_SCR_MGR_MAIN_MENU,
    UI_SCR_MGR_AQUARUM_MENU,
    UI_SCR_MGR_TEMPERATURE_MENU,
    UI_SCR_MGR_DATE_MENU,
    UI_SCR_MGR_SET_DATE_MENU,
    UI_SCR_MGR_SET_RTC_TIME_MENU,
    UI_SCR_MGR_SET_FEED_TIME_MENU,
    UI_SCR_MGR_SET_LIGHT_ON_TIME_MENU,
    UI_SCR_MGR_SET_LIGHT_OFF_TIME_MENU,
    UI_SCR_MGR_COUNT,
    UI_SCR_MGR_NULL = 0xFF
};

typedef Error (*_screenInit)();
typedef Error (*_screenRun)();
typedef Error (*_screenExit)();

Error UI_ScreenMgrRun();
Error UI_ScreenMgrInit();
Error UI_ScreenMgrRegisterScreen(enum UIScreenMgrScr screen, _screenInit initCbf, _screenRun runCbf, _screenExit exitCbf);
Error UI_ScreenMgrSwitch(enum UIScreenMgrScr screen);

#endif