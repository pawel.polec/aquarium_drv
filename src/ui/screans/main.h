#ifndef UI_MAIN_SCREEN_H_
#define UI_MAIN_SCREEN_H_

#include "framework/error.h"

Error UI_ScreenMainRun();
Error UI_ScreenMainInit();
Error UI_ScreenMainExit();
Error UI_ScreenMainRegisterScreen();

#endif