#include "display.h"
#include "config.h"

#include "framework/typedefs.h"

#include "drv/LCD/HD44780.h"
#include "drv/port/port.h"
#include "avr/pgmspace.h"
#include "drv/timer/timer.h"

static void _LCDInit() {

	LCD_Initalize();

    LCD_WriteCommand(HD44780_DDRAM_SET);
}

static void _turnDisplay(bool on){
    if (on) {
        PIN_SET(DISPLAY_ONOFF_PORT, DISPLAY_ONOFF_PIN, OUT_LO);
    } else {
        PIN_SET(DISPLAY_ONOFF_PORT, DISPLAY_ONOFF_PIN, OUT_HI);
    }
}

//should be controled by PWM
static void _turnBacklight(bool on){
    if (on) {
        PIN_SET(BACK_LIGHT_PORT, BACK_LIGHT_PIN, OUT_LO);
    } else {
        PIN_SET(BACK_LIGHT_PORT, BACK_LIGHT_PIN, OUT_HI);
    }
}

Error UI_displayInit() {
    PIN_CONFIG(DISPLAY_ONOFF_DDR, DISPLAY_ONOFF_PIN, PIN_OUTPUT);
    PIN_CONFIG(BACK_LIGHT_DDR, BACK_LIGHT_PIN, PIN_OUTPUT);
    _turnBacklight(false);

    _turnDisplay(true);

    _delay_ms(200);

    _LCDInit();

    _delay_ms(10);

    _turnBacklight(true);

    return NO_ERROR;
}


Error UI_displayTick() {

    return NO_ERROR;
}
