#ifndef UI_DISPLAY_H_
#define UI_DISPLAY_H_

#include "framework/error.h"

Error UI_displayInit();
Error UI_displayTick();

#endif
