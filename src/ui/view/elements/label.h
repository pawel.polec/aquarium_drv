#ifndef UI_LABEL_H_
#define UI_LABEL_H_

#include "framework/error.h"
#include "../ui.h"

enum ui_label_mode {
    UI_LABEL_MODE_STATIC,
    UI_LABEL_MODE_SCROLL,
    UI_LABEL_MODE_BLINKING,
    UI_LABEL_MODE_PAGES,
};

struct ui_label;

Error UI_LabelInit();

struct ui_label* UI_GetLabel(struct ui_pos *pos, uint8_t width, char* buffor, uint8_t buffSize);

Error UI_FreeLabel(struct ui_label* label);

Error UI_ShowLabel(struct ui_label* label);

Error UI_HideLabel(struct ui_label* label);

Error UI_RefreshLabel(struct ui_label* label);

#endif