#ifndef UI_SIMPLE_TEXT_H_
#define UI_SIMPLE_TEXT_H_

#include "framework/error.h"
#include "../ui.h"

void UI_SimpleText(struct ui_pos *pos, uint8_t width, const char* buffor);

#endif