#include "digit.h"

#include "drv/LCD/HD44780.h"
#include "ui/view/elements/custom_char.h"
#include "sm/kbd/kbd.h"

#define NUMBER_OF_DIGITS (8)

static struct UI_digit {
    struct ui_pos pos;

    struct UI_digit *next;
    struct UI_digit *prev;

    onEventCbf onIncreaseCbf;
    onEventCbf onDecreaseCbf;

    bool isInUse;

    uint8_t value;

} digits[NUMBER_OF_DIGITS] = {0};

static struct UI_digit *active = NULL;
static uint8_t signUp = 217;
static uint8_t signDown = 218;

static void _onKeyUp(enum KbdKey key) {
    if (active && active->onIncreaseCbf) {
        active->onIncreaseCbf();
    }
}

static void _onKeyDown(enum KbdKey key) {
    if (active && active->onDecreaseCbf) {
        active->onDecreaseCbf();
    }
}

static void _onKeyLeft(enum KbdKey key) {
    if (active && active->prev) {
        UI_digitSetActive(active->prev);
    }
}

static void _onKeyRight(enum KbdKey key) {
    if (active && active->next) {
        UI_digitSetActive(active->next);
    }
}

static void _deactivate(struct UI_digit* dig) {
    if (dig->pos.y > 0) {
        LCD_GoTo(dig->pos.x, dig->pos.y-1);
        LCD_WriteData(' ');
    }

    if (dig->pos.y < 3) {
        LCD_GoTo(dig->pos.x, dig->pos.y+1);
        LCD_WriteData(' ');
    }
    active = NULL;
}

static void _activate(struct UI_digit* dig) {
    if (dig->pos.y > 0) {
        uint8_t idx = UI_customCharGetCharIdx(CUSTOM_CHAR_BUTTON_UP);
        LCD_GoTo(dig->pos.x, dig->pos.y-1);
        LCD_WriteData((idx >= CUSTOMCHAR_SPACE) ? signUp : idx);
    }

    if (dig->pos.y < 3) {
        uint8_t idx = UI_customCharGetCharIdx(CUSTOM_CHAR_BUTTON_DOWN);
        LCD_GoTo(dig->pos.x, dig->pos.y+1);
        LCD_WriteData((idx >= CUSTOMCHAR_SPACE) ? signDown : idx);
    }
    active = dig;
}

static void _refreshValue(struct UI_digit* dig) {
    LCD_GoTo(dig->pos.x, dig->pos.y);
    LCD_WriteData('0'+dig->value);
}

struct UI_digit* UI_digitGet (struct ui_pos *pos, uint8_t value, onEventCbf onIncrease, onEventCbf onDecrease) {
    uint8_t i;

    for(i=0; i<NUMBER_OF_DIGITS; ++i) {
        if (digits[i].isInUse == false) {
            digits[i].isInUse = true;
            digits[i].pos = *pos;
            digits[i].next = NULL;
            digits[i].prev = NULL;
            digits[i].onIncreaseCbf = onIncrease;
            digits[i].onDecreaseCbf = onDecrease;
            digits[i].value = value;

            _refreshValue(&digits[i]);
            return &digits[i];          
        }
    }

    return NULL;
}


Error UI_digitInit () {
    SM_KbdSetCbfs(KBD_KEY_UP, _onKeyUp);
    SM_KbdSetCbfs(KBD_KEY_DOWN, _onKeyDown);
    SM_KbdSetCbfs(KBD_KEY_LEFT, _onKeyLeft);
    SM_KbdSetCbfs(KBD_KEY_RIGHT, _onKeyRight);

    return NO_ERROR;
}

Error UI_digitFree (struct UI_digit* dig) {
    if (!dig || !dig->isInUse) return ERROR_INVALID_PARAMETER;

    dig->isInUse = false;
    if (active == dig) _deactivate(dig);

    return NO_ERROR;
}

Error UI_digitFreeAll () {
    uint8_t i;
    for (i=0; i<NUMBER_OF_DIGITS; ++i) {
        UI_digitFree (&digits[i]);
    }

    return NO_ERROR;
}


Error UI_digitTanglePrev(struct UI_digit* dig, struct UI_digit* prev) {
    if (!dig || !dig->isInUse) return ERROR_INVALID_PARAMETER;
    if (!prev || !prev->isInUse) return ERROR_INVALID_PARAMETER;
    dig->prev = prev;
    prev->next = dig;

    return NO_ERROR;
}

Error UI_digitSetValue (struct UI_digit* dig, uint8_t value) {
    if (!dig || !dig->isInUse) return ERROR_INVALID_PARAMETER;
    if (value>9) return ERROR_INVALID_OPERATION;

    dig->value = value;

    _refreshValue(dig);

    return NO_ERROR;
}

Error UI_digitSetActive (struct UI_digit* dig) {
    if (!dig || !dig->isInUse) return ERROR_INVALID_PARAMETER;

    if (active != dig) {
        if (active != NULL) {
            _deactivate(active);
        }

        _activate(dig);
    }

    return NO_ERROR;
}

