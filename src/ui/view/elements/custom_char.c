
#include "custom_char.h"

#include "drv/LCD/HD44780.h"
#include "avr/pgmspace.h"
#include "drv/timer/timer.h"


#define CUSTOMCHAR_HEIGHT (8)

struct customChar {
    uint8_t framesCount;
    enum CCType {
        CUSTCHAR_NO_ANIM,
        CUSTCHAR_STRAIGHT,
        CUSTCHAR_BACK_AND_FORTH,
    } animType;
    struct CCFrames {
        uint8_t frame[CUSTOMCHAR_HEIGHT];
    } frames[];
};

const __flash struct customChar CCeatman = {
    .animType = CUSTCHAR_BACK_AND_FORTH,
    .framesCount = 6,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x18,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 0xf,
                (__flash const uint8_t) 0x1e,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x18,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1e,
                (__flash const uint8_t) 0xf,
                (__flash const uint8_t) 0
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0x1e,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1e,
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x0,
            }
        }
    }
};

const __flash struct customChar CCnull = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x18,
                (__flash const uint8_t) 0x14,
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0xa,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0x2,
                (__flash const uint8_t) 0x3,
            },
        }
    }
};

const __flash struct customChar CCsun = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0x15,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x15,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCmoon = {
    .animType = CUSTCHAR_STRAIGHT,
    .framesCount = 26,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x6,
                (__flash const uint8_t) 0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1d,
                (__flash const uint8_t) 0x1d,
                (__flash const uint8_t) 0xd,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 0x6,
                (__flash const uint8_t) 0xc,
                (__flash const uint8_t) 0x19,
                (__flash const uint8_t) 0x1b,
                (__flash const uint8_t) 0x1b,
                (__flash const uint8_t) 0xb,
                (__flash const uint8_t) 0x1,
                (__flash const uint8_t) 0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0x9,
                (__flash const uint8_t) 0x13,
                (__flash const uint8_t) 0x17,
                (__flash const uint8_t) 0x17,
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0x3,
                (__flash const uint8_t) 0x1,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x0,
                (__flash const uint8_t) 	0x2,
                (__flash const uint8_t) 	0x7,
                (__flash const uint8_t) 	0xf,
                (__flash const uint8_t) 	0xf,
                (__flash const uint8_t) 	0xf,
                (__flash const uint8_t) 	0x7,
                (__flash const uint8_t) 	0x3,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x1,
                (__flash const uint8_t) 	0x4,
                (__flash const uint8_t) 	0xe,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0xf,
                (__flash const uint8_t) 	0x7,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x3,
                (__flash const uint8_t) 	0x8,
                (__flash const uint8_t) 	0x1c,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0xe,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x7,
                (__flash const uint8_t) 	0x12,
                (__flash const uint8_t) 	0x18,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1d,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x6,
                (__flash const uint8_t) 	0x4,
                (__flash const uint8_t) 	0x11,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1b,
            },
        },
        {
            .frame = {
                (__flash const uint8_t)	0x4,
                (__flash const uint8_t)	0x9,
                (__flash const uint8_t)	0x3,
                (__flash const uint8_t)	0x1f,
                (__flash const uint8_t)	0x1f,
                (__flash const uint8_t)	0x1f,
                (__flash const uint8_t)	0x1f,
                (__flash const uint8_t)	0x17,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x1,
                (__flash const uint8_t) 	0x3,
                (__flash const uint8_t) 	0x7,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0xf,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x3,
                (__flash const uint8_t) 	0x7,
                (__flash const uint8_t) 	0xf,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x6,
                (__flash const uint8_t) 	0xf,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1e,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0xc,
                (__flash const uint8_t) 	0x1e,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1c,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x18,
                (__flash const uint8_t) 	0x1d,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x18,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x10,
                (__flash const uint8_t) 	0x1b,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1e,
                (__flash const uint8_t) 	0x10,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x0,
                (__flash const uint8_t) 	0x16,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1f,
                (__flash const uint8_t) 	0x1e,
                (__flash const uint8_t) 	0x1c,
                (__flash const uint8_t) 	0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x1,
                (__flash const uint8_t) 	0xc,
                (__flash const uint8_t) 	0x1e,
                (__flash const uint8_t) 	0x1e,
                (__flash const uint8_t) 	0x1e,
                (__flash const uint8_t) 	0x1c,
                (__flash const uint8_t) 	0x19,
                (__flash const uint8_t) 	0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x3,
                (__flash const uint8_t) 	0x18,
                (__flash const uint8_t) 	0x1c,
                (__flash const uint8_t) 	0x1c,
                (__flash const uint8_t) 	0x1c,
                (__flash const uint8_t) 	0x18,
                (__flash const uint8_t) 	0x13,
                (__flash const uint8_t) 	0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x7,
                (__flash const uint8_t) 	0x12,
                (__flash const uint8_t) 	0x18,
                (__flash const uint8_t) 	0x18,
                (__flash const uint8_t) 	0x18,
                (__flash const uint8_t) 	0x12,
                (__flash const uint8_t) 	0x7,
                (__flash const uint8_t) 	0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x7,
                (__flash const uint8_t) 	0x6,
                (__flash const uint8_t) 	0x14,
                (__flash const uint8_t) 	0x14,
                (__flash const uint8_t) 	0x14,
                (__flash const uint8_t) 	0x6,
                (__flash const uint8_t) 	0x7,
                (__flash const uint8_t) 	0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 	0x7,
                (__flash const uint8_t) 	0xe,
                (__flash const uint8_t) 	0xc,
                (__flash const uint8_t) 	0xc,
                (__flash const uint8_t) 	0xc,
                (__flash const uint8_t) 	0xe,
                (__flash const uint8_t) 	0x7,
                (__flash const uint8_t) 	0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0x0,
            },
        },
        {
            .frame = {
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0x0,
            },
        },
    }
};

const __flash struct customChar CCdrop = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0x1d,
                (__flash const uint8_t) 0x19,
                (__flash const uint8_t) 0xe,
            },
        }
    }
};

const __flash struct customChar CCtermo = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0xa,
                (__flash const uint8_t) 0xa,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0xe,
            },
        }
    }
};

const __flash struct customChar CCcelcDeg = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x8,
                (__flash const uint8_t) 0x14,
                (__flash const uint8_t) 0x8,
                (__flash const uint8_t) 0x3,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0x3,
                (__flash const uint8_t) 0x20,
            },
        }
    }
};

const __flash struct customChar CCfire = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x2,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0xc,
                (__flash const uint8_t) 0x1a,
                (__flash const uint8_t) 0x15,
                (__flash const uint8_t) 0x19,
                (__flash const uint8_t) 0x1b,
                (__flash const uint8_t) 0xe,
            },
        }
    }
};

const __flash struct customChar CCcapitalSWithAcuteAccent = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0xc,
                (__flash const uint8_t) 0x2,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCSWithAcuteAccent = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x2,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x10,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1,
                (__flash const uint8_t) 0x1e,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCcapitalOWithAcuteAccent = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x2,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x15,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCOWithAcuteAccent = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x2,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCCapitalEWithOgonek = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x2,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x10,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1,
                (__flash const uint8_t) 0x1e,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCEWithOgonek = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0x10,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x3,
            },
        }
    }
};

const __flash struct customChar CCcapitalNWithAcuteAccent = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x15,
                (__flash const uint8_t) 0x15,
                (__flash const uint8_t) 0x19,
                (__flash const uint8_t) 0x15,
                (__flash const uint8_t) 0x13,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCNWithAcuteAccent = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x2,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0x16,
                (__flash const uint8_t) 0x19,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0x11,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCCapitalLWithStroke = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x10,
                (__flash const uint8_t) 0x10,
                (__flash const uint8_t) 0x14,
                (__flash const uint8_t) 0x18,
                (__flash const uint8_t) 0x10,
                (__flash const uint8_t) 0x10,
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCLWithStroke = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0xc,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0x6,
                (__flash const uint8_t) 0xc,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCButtonUp = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x1f,
            },
        }
    }
};

const __flash struct customChar CCButtonDown = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x1f,
                (__flash const uint8_t) 0xe,
                (__flash const uint8_t) 0x4,
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0x0,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCButtonLeft = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x1,
                (__flash const uint8_t) 0x3,
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0xf,
                (__flash const uint8_t) 0x7,
                (__flash const uint8_t) 0x3,
                (__flash const uint8_t) 0x1,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar CCButtonRight = {
    .animType = CUSTCHAR_NO_ANIM,
    .framesCount = 1,
    .frames = {
        {
            .frame = {
                (__flash const uint8_t) 0x10,
                (__flash const uint8_t) 0x18,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x1e,
                (__flash const uint8_t) 0x1c,
                (__flash const uint8_t) 0x18,
                (__flash const uint8_t) 0x10,
                (__flash const uint8_t) 0x0,
            },
        }
    }
};

const __flash struct customChar *allCC[] = {
    &CCnull,
    &CCsun,
    &CCmoon,
    &CCdrop,
    &CCtermo,
    &CCeatman,
    &CCcelcDeg,
    &CCfire,
    &CCcapitalSWithAcuteAccent,
    &CCSWithAcuteAccent,
    &CCcapitalOWithAcuteAccent,
    &CCOWithAcuteAccent,
    &CCCapitalEWithOgonek,
    &CCEWithOgonek,
    &CCcapitalNWithAcuteAccent,
    &CCNWithAcuteAccent,
    &CCCapitalLWithStroke,
    &CCLWithStroke,
    &CCButtonUp,
    &CCButtonDown,
    &CCButtonLeft,
    &CCButtonRight,
};

enum CustomChar charSet[CUSTOMCHAR_SPACE] = {
    CUSTOM_CHAR_NULL,
    CUSTOM_CHAR_SUN,
    CUSTOM_CHAR_MOON,
    CUSTOM_CHAR_DROP,
    CUSTOM_CHAR_THERMO,
    CUSTOM_CHAR_EATMAN,
    CUSTOM_CHAR_CELS_DEG,
    CUSTOM_CHAR_FIRE
};

volatile static bool animNextChar = false;

static void _triggerAnimChar() {
    animNextChar = true;
}

static Error _startAnimChar() {
    struct drv_timer tim = {
        .type = DRV_TIMER_INTERVAL,
        .cbf = _triggerAnimChar,
        .ticks10ms = 15,
    };
    return Drv_TimerRegister(&tim);
}

Error UI_customCharSetCharset(enum CustomChar charset[CUSTOMCHAR_SPACE]) {
    uint8_t cs = 0;
    for (cs = 0; cs<CUSTOMCHAR_SPACE; ++cs) {
        charSet[cs] = charset[cs];
    }

    LCD_WriteCommand(HD44780_CGRAM_SET);
    {
        int i=0, j=0;
        for (; i< CUSTOMCHAR_SPACE; ++i)
            for (j=0;j<CUSTOMCHAR_HEIGHT; ++j)
		        LCD_WriteData(allCC[charSet[i]]->frames[0].frame[j]);
    }

    return NO_ERROR;
}

Error UI_customCharSetChar(uint8_t idx, enum CustomChar ch) {
    if (idx >= CUSTOMCHAR_SPACE) return ERROR_INVALID_PARAMETER;

    charSet[idx] = ch;

    LCD_WriteCommand(HD44780_CGRAM_SET|CUSTOMCHAR_HEIGHT*idx);
    {
        int in=0;
        for (; in<CUSTOMCHAR_HEIGHT; ++in)
            LCD_WriteData(allCC[charSet[idx]]->frames[0].frame[in]);
    }

    return NO_ERROR;
}

uint8_t UI_customCharGetCharIdx(enum CustomChar ch) {
    uint8_t cs = 0;
    for (cs = 0; cs<CUSTOMCHAR_SPACE; ++cs) {
        if (charSet[cs] == ch) return cs;
    }

    return CUSTOM_CHAR_OUT_OF_BOUND;
}

Error UI_customCharInit() {
    LCD_WriteCommand(HD44780_CGRAM_SET);
    {
        int i=0, j=0;
        for (; i< CUSTOMCHAR_SPACE; ++i)
            for (j=0;j<CUSTOMCHAR_HEIGHT; ++j)
		        LCD_WriteData(allCC[charSet[i]]->frames[0].frame[j]);
    }

    return _startAnimChar();
}

Error UI_customCharTick() {
    static uint8_t framesIter[CUSTOMCHAR_SPACE] = {0};

    if (animNextChar) {
        uint8_t cs = 0;

        for (cs = 0; cs<CUSTOMCHAR_SPACE; ++cs) {
            if (allCC[charSet[cs]]->animType != CUSTCHAR_NO_ANIM) {
                uint8_t frame;
                framesIter[cs]=framesIter[cs]%allCC[charSet[cs]]->framesCount;
                frame = framesIter[cs];
                if (allCC[charSet[cs]]->animType == CUSTCHAR_BACK_AND_FORTH) {
                    uint8_t size = ((allCC[charSet[cs]]->framesCount) >> 1) + 1;
                    if (size <= framesIter[cs]) {
                        frame = allCC[charSet[cs]]->framesCount - framesIter[cs];
                    }
                }

                LCD_WriteCommand(HD44780_CGRAM_SET|CUSTOMCHAR_HEIGHT*cs);
                {
                    int in=0;
                    for (; in<CUSTOMCHAR_HEIGHT; ++in)
                        LCD_WriteData(allCC[charSet[cs]]->frames[frame].frame[in]);
                }
                ++framesIter[cs];
            }
        }

        animNextChar = false;
    }

    return NO_ERROR;
}
