#ifndef UI_SCROLL_BAR_H_
#define UI_SCROLL_BAR_H_

#include "framework/error.h"
#include "../ui.h"

void UI_ScrollBarInit(struct ui_pos *pos, uint8_t height, uint8_t max, uint8_t value);
void UI_ScrollBarSetValue(uint8_t value);
void UI_ScrollBarRelease();

#endif