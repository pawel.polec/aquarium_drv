#ifndef UI_DIGIT_H_
#define UI_DIGIT_H_

#include "framework/error.h"
#include "ui/view/ui.h"

struct UI_digit;

typedef void (*onEventCbf)();

struct UI_digit* UI_digitGet (struct ui_pos *pos, uint8_t value, onEventCbf onIncrease, onEventCbf onDecrease);
Error UI_digitInit ();
Error UI_digitFree (struct UI_digit* dig);
Error UI_digitFreeAll ();
Error UI_digitTanglePrev(struct UI_digit* dig, struct UI_digit* prev);
Error UI_digitSetValue (struct UI_digit* dig, uint8_t value);
Error UI_digitSetActive (struct UI_digit* dig);

#endif