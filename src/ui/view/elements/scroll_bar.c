#include "scroll_bar.h"
#include "drv/LCD/HD44780.h"

#define SB_INDICATOR_CH '\xFF'
#define SB_BACKGROUND_CH '!'

static struct _sb {
    struct ui_pos pos;
    uint8_t height;

    uint8_t max;
    uint8_t value;
} instance = {0};

static void _draw() {
    uint8_t indPos = (instance.value * (instance.height-1)) / (instance.max);
    if (instance.value == instance.max) indPos = instance.height-1;
    uint8_t i;
    for (i=0; i<instance.height; ++i) {
        LCD_GoTo(instance.pos.x, instance.pos.y+i);
        LCD_WriteData((indPos == i ) ? SB_INDICATOR_CH : SB_BACKGROUND_CH);
    }
}

void UI_ScrollBarInit(struct ui_pos *pos, uint8_t height, uint8_t max, uint8_t value) {
    if (pos == NULL) return;
    if (max == 0) return;
    instance.pos = *pos;
    instance.height = height;
    instance.max = max;
    instance.value = value;

    _draw();
}

void UI_ScrollBarSetValue(uint8_t value) {
    instance.value = value;
    _draw();
}
