#include "simple_text.h"
#include "drv/LCD/HD44780.h"

void UI_SimpleText(struct ui_pos *pos, uint8_t width, const char* buffor) {
    LCD_GoTo (pos->x, pos->y);
    LCD_WriteTextP(buffor);
}