#include "label.h"
#include "framework/typedefs.h"
#include "drv/LCD/HD44780.h"

#define UI_LABELS_MAX (10)

struct ui_label {
    struct ui_pos pos;
    uint8_t width;

    enum ui_label_mode mode;
    bool isVisible;

    char* buffor;
    uint8_t buffSize;

    union ui_label_state
    {
        struct ui_label_scroll {
            uint8_t position;
        } scroll;
        struct ui_label_pages {
            uint8_t page;
        } pages;
    } state;
    

    bool isAlocated;
};

struct ui_label labels[UI_LABELS_MAX] = {0};

static Error _refresh(struct ui_label* label) {
    char tmp[21] = {0};

    LCD_GoTo(label->pos.x, label->pos.y);
    snprintf(tmp, MIN(21, label->width+1), "%s                    ", label->buffor);
    LCD_WriteText(tmp);

    return NO_ERROR;
}

Error UI_LabelInit() {
    
    return NO_ERROR;
};

struct ui_label* UI_GetLabel(struct ui_pos *pos, uint8_t width, char* buffor, uint8_t buffSize) {
    struct ui_label* ret = NULL;
    uint8_t i;
    
    if (pos == NULL) return NULL;
    if (buffor == NULL) return NULL;

    for (i=0; i<UI_LABELS_MAX; ++i) {
        if (!labels[i].isAlocated) {
            labels[i].isAlocated = true;
            labels[i].pos = *pos;
            labels[i].width = width;
            labels[i].buffor = buffor;
            labels[i].buffSize = buffSize;
            ret = &labels[i];
            break;
        }
    }
    return ret;
}

Error UI_FreeLabel(struct ui_label* label) {
    Error ret = NO_ERROR;
    if (label == NULL) return ERROR_INVALID_PARAMETER;
    
    label->buffor = NULL;
    label->buffSize = 0;
    label->isAlocated = false;

    return ret;
}

Error UI_ShowLabel(struct ui_label* label) {
    if (label == NULL) return ERROR_INVALID_PARAMETER;
    label->isVisible = true;

    return _refresh(label);
}

Error UI_HideLabel(struct ui_label* label) {
    if (label == NULL) return ERROR_INVALID_PARAMETER;
    label->isVisible = false;

    return _refresh(label);
}

Error UI_RefreshLabel(struct ui_label* label) {
    if (label == NULL) return ERROR_INVALID_PARAMETER;
    return _refresh(label);
}
