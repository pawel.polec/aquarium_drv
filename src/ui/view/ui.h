#ifndef UI_VIEW_H_
#define UI_VIEW_H_

#include "framework/error.h"
#include "framework/typedefs.h"
#include "drv/kbd/kbd.h"

struct ui_pos {
    uint8_t x;
    uint8_t y;
};

struct ui_size {
    uint8_t width;
    uint8_t height;
};

#endif