#include "temperature.h"
#include "drv/1wire/1wire.h"
#include "drv/LCD/HD44780.h"
#include "avr/pgmspace.h"

#define TEMP_LIST_SIZE (4)
#define SM_TEMP_CBFS_LIST_SIZE (4)

enum sm_temp_states {
    SM_TEMP_INIT,
    SM_TEMP_SEARCH,
    SM_TEMP_START_READ,
    SM_TEMP_READ,
    SM_TEMP_END_READ,
    SM_TEMP_WAIT,
    SM_TEMP_DISPAY,
    SM_TEMP_NULL
};

struct sm_temp {
    enum sm_temp_states currentState;
    enum sm_temp_states nextState;
    struct OW_device list[TEMP_LIST_SIZE];
    uint16_t measurments[TEMP_LIST_SIZE];
    onTempRead cbfs[SM_TEMP_CBFS_LIST_SIZE];
    uint8_t listCnt;
    uint8_t curentIdx;
};

static struct sm_temp sm = {0};

static Error _state_search() {
    Error ret = NO_ERROR;

    sm.listCnt = TEMP_LIST_SIZE;
    ret = OW_SearchRom(sm.list, &(sm.listCnt), OW_Family_NULL);
    if (ret != NO_ERROR) {
        sm.nextState = SM_TEMP_INIT;
    } else if (sm.listCnt < 1) {
        sm.nextState = SM_TEMP_SEARCH;
    } else {
        sm.nextState = SM_TEMP_START_READ;
        if (sm.listCnt > TEMP_LIST_SIZE) sm.listCnt = TEMP_LIST_SIZE;
    }

    return ret;
}

static Error _state_read() {
    Error ret = NO_ERROR;

    ret = OW_DS18x20_StartConversion( &sm.list[ sm.curentIdx ] );
    if (ret != NO_ERROR) {
        sm.nextState = SM_TEMP_INIT;
    } else {
        sm.nextState = SM_TEMP_WAIT;
    }

    return ret;
}

static Error _state_wait() {
    Error ret = NO_ERROR;

    if (OW_DS18x20_LastConversionIsDone()) {
        sm.nextState = SM_TEMP_END_READ;
    } else {
        sm.nextState = SM_TEMP_WAIT;
    }

    return ret;
}

static Error _state_readEnd() {
    Error ret = NO_ERROR;

    uint16_t tempStamp;
    ret = OW_DS18x20_ReadTemp(&sm.list[ sm.curentIdx ], &tempStamp);

    if (ret != NO_ERROR) {
        sm.nextState = SM_TEMP_INIT;
    } else {
        int8_t temp;
        uint8_t rest=100; 
        if (sm.list[ sm.curentIdx ].dev.laseredRom.family == OW_Family_DS18B20) {
            OW_DS18b20_ConvertTemp(tempStamp, &temp, &rest);
        } else if (sm.list[ sm.curentIdx ].dev.laseredRom.family == OW_Family_DS1820) {
            OW_DS1820_ConvertTemp(tempStamp, &temp, &rest);
        }
        
        if (rest < 100) {
            sm.measurments[ sm.curentIdx ] = (100*temp + rest);
        } else {
            sm.measurments[ sm.curentIdx ] = 0xFFFF;
        }

        //if (sm.cbfs[ sm.curentIdx ]) sm.cbfs[ sm.curentIdx ](sm.curentIdx);
        {   
            uint8_t idx;

            for (idx=0; idx<SM_TEMP_CBFS_LIST_SIZE; ++idx) {
                if (sm.cbfs[idx] != NULL) {
                    sm.cbfs[idx](sm.curentIdx);
                }
            }
        }

        if (sm.curentIdx+1 >= sm.listCnt) {
            sm.nextState = SM_TEMP_SEARCH;
        } else {
            ++sm.curentIdx;
            sm.nextState = SM_TEMP_START_READ;
        }
    }

    return ret;
}

static Error _state_machine() {
    Error ret = NO_ERROR;

    switch (sm.currentState)
    {
        case SM_TEMP_INIT:
            sm.listCnt=0;
            sm.nextState = SM_TEMP_SEARCH;
            break;

        case SM_TEMP_SEARCH:
            ret = _state_search(sm);
            break;

        case SM_TEMP_START_READ:
            sm.curentIdx = 0;
            sm.nextState = SM_TEMP_READ;
            break;

        case SM_TEMP_READ:
            ret = _state_read(sm);
            break;

        case SM_TEMP_WAIT:
            ret = _state_wait(sm);
            break;

        case SM_TEMP_END_READ:
            ret = _state_readEnd(sm);
            break;
        
        default:
            return ERROR_INVALID_OPERATION;
            break;
    }

    sm.currentState = sm.nextState;
    sm.nextState = SM_TEMP_NULL;

    return ret;
}

void SM_TempInit() {
    OW_Initialize();
    sm.currentState = SM_TEMP_INIT;
    sm.nextState = SM_TEMP_NULL;
}

Error SM_TempRun() {

    return _state_machine();
}

int16_t SM_TempGet(uint8_t index) {
    if (sm.listCnt<=index) return ERROR_INVALID_PARAMETER;

    return sm.measurments[index];
}

Error SM_TempRegister(onTempRead cbf) {
    uint8_t idx;
    if (!cbf) return ERROR_INVALID_PARAMETER;

    for (idx=0; idx<SM_TEMP_CBFS_LIST_SIZE; ++idx) {
        if (sm.cbfs[idx] == NULL) {
            sm.cbfs[idx] = cbf;
            return NO_ERROR;
        }
    }
    return ERROR_INSUFFICIENT_RESOURCES;
}

Error SM_TempUnregister(onTempRead cbf) {
    uint8_t idx;
    if (!cbf) return ERROR_INVALID_PARAMETER;

    for (idx=0; idx<SM_TEMP_CBFS_LIST_SIZE; ++idx) {
        if (sm.cbfs[idx] == cbf) {
            sm.cbfs[idx] = NULL;
            return NO_ERROR;
        }
    }
    return ERROR_INVALID_PARAMETER;
}
