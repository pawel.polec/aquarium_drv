#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_

#include "framework/error.h"

typedef void (*onTempRead) (uint8_t index);
struct sm_temp;

void SM_TempInit();

Error SM_TempRun();

Error SM_TempRegister(onTempRead cbf);

Error SM_TempUnregister(onTempRead cbf);

int16_t SM_TempGet(uint8_t index) ;

#endif
