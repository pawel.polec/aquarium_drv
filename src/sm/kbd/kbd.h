#ifndef SM_KBD_H_
#define SM_KBD_H_

#include "framework/error.h"
#include "drv/kbd/kbd.h"

struct sm_kbd;

typedef void (*SMKbdCbf)(enum KbdKey key);

struct sm_kbd* SM_KbdInit();

Error SM_KbdRun(struct sm_kbd * sm);
Error SM_KbdSetCbfs(enum KbdKey key, SMKbdCbf cbf);
Error SM_KbdClearCbfs();

#endif
