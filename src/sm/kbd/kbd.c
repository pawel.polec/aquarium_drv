#include "kbd.h"

//Temporary includes:

#include "drv/peripheral/peripherials.h"
#include "drv/rtc/rtc.h"
#include <string.h>


enum sm_kbd_states {
    SM_KBD_INIT,
    SM_KBD_NULL
};

struct sm_kbd {
    enum sm_kbd_states currentState;
    enum sm_kbd_states nextState;
};

static struct sm_kbd instance = {0};

SMKbdCbf _cbfsArray[KBD_KEY_COUNT] = {0};

static void _onKeyCbf (enum KbdKey key, enum KbdState state) {
	if (state == KBD_STATE_KEY_DOWN) {
		if (_cbfsArray[key]) _cbfsArray[key](key);
	}
}

struct sm_kbd* SM_KbdInit() {
    static struct Kbd kbd = {
		.matrix = {
				{KBD_KEY_UNDEFINIED, KBD_KEY_DOWN,   KBD_KEY_CANCEL,   KBD_KEY_UNDEFINIED},
				{KBD_KEY_RIGHT,      KBD_KEY_ENTER,  KBD_KEY_LEFT,     KBD_KEY_UNDEFINIED},
				{KBD_KEY_MENU,       KBD_KEY_UP,     KBD_KEY_FEEDER,   KBD_KEY_UNDEFINIED},
				{KBD_KEY_NIGHTLIGHT, KBD_KEY_PUMP,   KBD_KEY_DAYLIGHT, KBD_KEY_UNDEFINIED},
		},
		.callback = _onKeyCbf,
	};

	Kbd_Initialize();

	Kbd_Register(kbd, 0);

    return &instance;
}

Error SM_KbdRun(struct sm_kbd * sm) {
    return Kbd_Scan();
}

Error SM_KbdSetCbfs(enum KbdKey key, SMKbdCbf cbf) {
	if (key >= KBD_KEY_COUNT) return ERROR_INVALID_PARAMETER;

	_cbfsArray[key] = cbf;
    return NO_ERROR;
}

Error SM_KbdClearCbfs() {
	memset(&_cbfsArray, 0, sizeof(_cbfsArray));
    return NO_ERROR;
}