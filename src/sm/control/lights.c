// 2 state or PID? We'll see (or maybe 3 state)

#include "control.h"
#include "framework/db/db.h"
#include "drv/peripheral/peripherials.h"
#include "drv/rtc/rtc.h"

#define SM_TEMP_HEAT_CFB_COUNT (2)

enum sm_ctrl_light_states {
    SM_CTRL_LIGHT_INIT,
    SM_CTRL_LIGHT_REINIT,
    SM_CTRL_LIGHT_RUNNING,
    SM_CTRL_LIGHT_NULL
};

static struct ctrlLights {
    heaterStateChanged heaterCbf[SM_TEMP_HEAT_CFB_COUNT];
    enum sm_ctrl_light_states currentState;
    enum sm_ctrl_light_states nextState;

    time_t lightsOn;
    time_t lightsOff;
} instance = {0};

static void _alarmLightsOn() {
    Peripherials_Set(PERIPH_DAYLIGHT, PERIPH_STATE_ON);
}

static void _alarmLightsOff() {
    Peripherials_Set(PERIPH_DAYLIGHT, PERIPH_STATE_OFF);
}

static void _evaluateState() {
    struct tm currentTime = {0};
    RTC_GetTime(&currentTime);

    time_t ct = (long)ONE_HOUR*currentTime.tm_hour + currentTime.tm_min*60 + currentTime.tm_sec;

    if (instance.lightsOn < instance.lightsOff) {
        if (ct > instance.lightsOn && ct < instance.lightsOff) {
            Peripherials_Set(PERIPH_DAYLIGHT, PERIPH_STATE_ON);
        } else {
            Peripherials_Set(PERIPH_DAYLIGHT, PERIPH_STATE_OFF);
        }
    } else {
         if (ct < instance.lightsOn && ct > instance.lightsOff) {
            Peripherials_Set(PERIPH_DAYLIGHT, PERIPH_STATE_OFF);
        } else {
            Peripherials_Set(PERIPH_DAYLIGHT, PERIPH_STATE_ON);
        }       
    }



}

static void _loadFromDb() {
    DBInstance *db;
    DB_GetDB(&db);

    if (ERROR_NOT_FOUND ==  DB_GetEntry(db, DB_FIELD_ID_LIGHT_ON, (uint8_t*) &instance.lightsOn, sizeof(instance.lightsOn))) {
        instance.lightsOn = (long)ONE_HOUR*7+60*10;
    }

    if (ERROR_NOT_FOUND ==  DB_GetEntry(db, DB_FIELD_ID_LIGHT_OFF, (uint8_t*) &instance.lightsOff, sizeof(instance.lightsOff))) {
        instance.lightsOff = (long)ONE_HOUR*21;
    }
}

static void _subscribeAllAlarms() {
    struct RTC_alarm alarm = {
        .time = instance.lightsOn ,
        .alarmCbf = _alarmLightsOn,
    };
    RTC_RegisterAlarm (alarm);

    alarm.time = instance.lightsOff;
    alarm.alarmCbf = _alarmLightsOff;
    RTC_RegisterAlarm (alarm);
}

static void _unsubscribeAllAlarms() {
    RTC_UnregisterAlarm ((struct RTC_alarm ) {.time = instance.lightsOn , .alarmCbf = _alarmLightsOn});
    RTC_UnregisterAlarm ((struct RTC_alarm ) {.time = instance.lightsOff , .alarmCbf = _alarmLightsOff});
}

static void _onDBChanged() {
    instance.nextState = SM_CTRL_LIGHT_REINIT;
}

static Error _sm() {
    switch (instance.currentState)
    {
        case SM_CTRL_LIGHT_INIT:
            _loadFromDb();
            _evaluateState();
            _subscribeAllAlarms();
            RTC_RegisterOnTimeChangeCbf(_evaluateState);
            instance.nextState = SM_CTRL_LIGHT_RUNNING;
            break;

        case SM_CTRL_LIGHT_REINIT:
        {
            RTC_UnregisterOnTimeChangeCbf(_evaluateState);
            _unsubscribeAllAlarms();
            instance.nextState = SM_CTRL_LIGHT_INIT;
            break;
        }

        default:
            break;
    }

    if (instance.nextState != SM_CTRL_LIGHT_NULL) {
        instance.currentState = instance.nextState;
        instance.nextState = SM_CTRL_LIGHT_NULL;
    }

    return NO_ERROR;
}

Error SM_CtrlLightsInit() {
    DBInstance *db;
    DB_GetDB(&db);
    DB_OnChangeRegister (db, DB_FIELD_ID_LIGHT_ON, _onDBChanged);
    DB_OnChangeRegister (db, DB_FIELD_ID_LIGHT_OFF, _onDBChanged);

    instance.currentState = SM_CTRL_LIGHT_INIT;

    return NO_ERROR;
}

Error SM_CtrlLightsTick() {
    return _sm();
}