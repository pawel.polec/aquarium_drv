#ifndef SM_CONTROL_H_
#define SM_CONTROL_H_

#include "framework/error.h"

enum sm_heater_state {
    SM_HEATER_OFF,
    SM_HEATER_ON,
};

typedef void (*heaterStateChanged)(enum sm_heater_state state);

Error SM_CtrlTempInit();

Error SM_CtrlTempHeaterRegister(heaterStateChanged cbf);

Error SM_CtrlTempHeaterUnregister(heaterStateChanged cbf);

Error SM_CtrlTempTick();


Error SM_CtrlLightsInit();

Error SM_CtrlLightsTick();


Error SM_CtrlFeedsInit();

Error SM_CtrlFeedsTick();

#endif