#include "control.h"
#include "drv/timer/timer.h"
#include "framework/db/db.h"
#include "drv/peripheral/peripherials.h"
#include "drv/rtc/rtc.h"

#include "avr/pgmspace.h"
#include <string.h>

enum sm_ctrl_feed_states {
    SM_CTRL_FEED_INIT,
    SM_CTRL_FEED_REINIT,
    SM_CTRL_FEED_RUNNING,
    SM_CTRL_FEED_NULL
};

static struct ctrlFeeds {
    enum sm_ctrl_feed_states currentState;
    enum sm_ctrl_feed_states nextState;

    time_t feedTime;
} instance = {0};

static void _feederOff() {
    Peripherials_Set(PERIPH_FEEDER, PERIPH_STATE_OFF);
}

static void _feederOn() {
    struct drv_timer tim = {
        .type = DRV_TIMER_ONE_TIME,
        .cbf = _feederOff,
        .ticks10ms = 150,
    };
    Drv_TimerRegister(&tim);
    Peripherials_Set(PERIPH_FEEDER, PERIPH_STATE_ON);

}

static void _onDBChanged() {
    instance.nextState = SM_CTRL_FEED_REINIT;
}

static void _loadFromDb() {
    DBInstance *db;
    DB_GetDB(&db);

    if (ERROR_NOT_FOUND ==  DB_GetEntry(db, DB_FIELD_ID_FEED_TIME, (uint8_t*) &instance.feedTime, sizeof(instance.feedTime))) {
        instance.feedTime = (long)ONE_HOUR*19;
    }
}

static void _subscribeAllAlarms() {
    struct RTC_alarm alarm = {
        .time = instance.feedTime ,
        .alarmCbf = _feederOn,
    };
    RTC_RegisterAlarm (alarm);
}

static Error _sm() {
    switch (instance.currentState)
    {
        case SM_CTRL_FEED_INIT:
            _loadFromDb();
            _subscribeAllAlarms();
            instance.nextState = SM_CTRL_FEED_RUNNING;
            break;

        case SM_CTRL_FEED_REINIT:
        {
            RTC_UnregisterAlarm((struct RTC_alarm) {.time = instance.feedTime ,.alarmCbf = _feederOn, });
            instance.nextState = SM_CTRL_FEED_INIT;
            break;
        }
        default:
            break;
    }

    if (instance.nextState != SM_CTRL_FEED_NULL) {
        instance.currentState = instance.nextState;
        instance.nextState = SM_CTRL_FEED_NULL;
    }

    return NO_ERROR;
}

Error SM_CtrlFeedsInit() {
    DBInstance *db;
    DB_GetDB(&db);
    DB_OnChangeRegister (db, DB_FIELD_ID_FEED_TIME, _onDBChanged);

    instance.currentState = SM_CTRL_FEED_INIT;

    return NO_ERROR;
}

Error SM_CtrlFeedsTick() {
    return _sm();
}