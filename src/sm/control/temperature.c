// 2 state or PID? We'll see (or maybe 3 state)

#include "control.h"
#include "framework/db/db.h"
#include "sm/sensor/temperature.h"
#include "drv/peripheral/peripherials.h"

#define SM_TEMP_HEAT_CFB_COUNT (2)

enum sm_ctrl_temp_states {
    SM_CTRL_TEMP_INIT,
    SM_CTRL_TEMP_WAIT_FOR_MEAS,
    SM_CTRL_TEMP_HEATING,
    SM_CTRL_TEMP_COOLING,
    SM_CTRL_TEMP_NULL
};

static struct ctrlTemperature {
    heaterStateChanged heaterCbf[SM_TEMP_HEAT_CFB_COUNT];
    enum sm_heater_state heaterState;
    enum sm_ctrl_temp_states currentState;
    enum sm_ctrl_temp_states nextState;

    uint16_t histeresis;
    uint16_t tSet;
    uint16_t measurment;
} instance;

static void _heaterNotify (enum sm_heater_state state) {
    uint8_t i;
    for (i=0; i<SM_TEMP_HEAT_CFB_COUNT; ++i) {
        if (instance.heaterCbf[i] != NULL){
            instance.heaterCbf[i](state);
        }
    }
}

static void _temperatureCbf (uint8_t index) {
    instance.measurment = SM_TempGet(0);

    if (instance.currentState == SM_CTRL_TEMP_WAIT_FOR_MEAS) {
        instance.currentState = SM_CTRL_TEMP_COOLING;
    }
}

static void _loadFromDb() {
    DBInstance *db;
    DB_GetDB(&db);

    if (ERROR_NOT_FOUND ==  DB_GetEntry(db, DB_FIELD_ID_TMP_SET, (uint8_t*) &instance.tSet, sizeof(instance.tSet))) {
        instance.tSet = 2400;
    }

    if (ERROR_NOT_FOUND ==  DB_GetEntry(db, DB_FIELD_ID_TMP_HIST, (uint8_t*) &instance.tSet, sizeof(instance.tSet))) {
        instance.histeresis = 40;
    }
}

static void _coolig() { //instance.histeresis/2
    if (instance.tSet - (instance.histeresis>>1) > instance.measurment) {
        instance.nextState = SM_CTRL_TEMP_HEATING;
        Peripherials_Set(PERIPH_HEATER1, PERIPH_STATE_ON);
        _heaterNotify(SM_HEATER_ON);
    }
}

static void _heating() {//instance.histeresis/2
    if (instance.tSet + (instance.histeresis>>1) < instance.measurment) {
        instance.nextState = SM_CTRL_TEMP_COOLING;
        Peripherials_Set(PERIPH_HEATER1, PERIPH_STATE_OFF);
        _heaterNotify(SM_HEATER_OFF);
    }
}

static Error _sm() {
    switch (instance.currentState)
    {
        case SM_CTRL_TEMP_INIT:
            _loadFromDb();
            instance.nextState = SM_CTRL_TEMP_WAIT_FOR_MEAS;
            break;

        case SM_CTRL_TEMP_WAIT_FOR_MEAS:
            break;

        case SM_CTRL_TEMP_COOLING:
            _coolig();
            break;

        case SM_CTRL_TEMP_HEATING:
            _heating();
            break;
        
        default:
            break;
    }
    //
    if (instance.nextState != SM_CTRL_TEMP_NULL) {
        instance.currentState = instance.nextState;
        instance.nextState = SM_CTRL_TEMP_NULL;
    }

    return NO_ERROR;
}

Error SM_CtrlTempInit() {
    uint8_t i;
    for (i=0; i<SM_TEMP_HEAT_CFB_COUNT; ++i) {
        instance.heaterCbf[i] = NULL;
    }
    
    SM_TempRegister(_temperatureCbf);

    return NO_ERROR;
}

Error SM_CtrlTempHeaterRegister(heaterStateChanged cbf) {
    uint8_t i;
    for (i=0; i<SM_TEMP_HEAT_CFB_COUNT; ++i) {
        if (instance.heaterCbf[i] == NULL){
            instance.heaterCbf[i] = cbf;

            _heaterNotify((instance.currentState == SM_CTRL_TEMP_HEATING) ? SM_HEATER_ON : SM_HEATER_OFF);
            return NO_ERROR;
        }
    }
    return ERROR_INSUFFICIENT_RESOURCES;
}

Error SM_CtrlTempHeaterUnregister(heaterStateChanged cbf) {
    uint8_t i;
    for (i=0; i<SM_TEMP_HEAT_CFB_COUNT; ++i) {
        if (instance.heaterCbf[i] == cbf){
            instance.heaterCbf[i] = NULL;
            return NO_ERROR;
        }
    }
    return ERROR_INVALID_PARAMETER;
}

Error SM_CtrlTempTick() {
    return _sm();
}