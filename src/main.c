/*
 * main.c
 *
 *  Created on: 23 cze 2015
 *      Author: Paweł Połeć
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <string.h>

#include "framework/typedefs.h"

#include "drv/timer/timer.h"
#include "drv/LCD/HD44780.h"
#include "drv/peripheral/peripherials.h"
#include "drv/kbd/kbd.h"
#include "drv/port/port.h"
#include "drv/rtc/rtc.h"
#include "drv/1wire/1wire.h"
#include "drv/uart/uart.h"

#include "framework/db/db.h"

#include "sm/sensor/temperature.h"
#include "sm/kbd/kbd.h"
#include "sm/control/control.h"

#include "ui/display/display.h"
#include "ui/view/ui.h"
#include "ui/view/elements/label.h"
#include "ui/view/elements/custom_char.h"
#include "ui/screans/main.h"
#include "ui/screans/menu.h"
#include "ui/screans/date_set.h"
#include "ui/screans/time_set.h"
#include "ui/screans/manager.h"

bool updateTime = false;
bool newLine = false;

int main (void) {

	wdt_enable( WDTO_1S );

	// __flash const static char text[81]  =
    //               "Pt20.09.21  Odczyty "
	//               "\4""24,5\6 \5""22:33 \7 ON  "
	//               " 12:34:56  T1:23,45 "
	// 	          "\1"" 7:00 \2""21:03 \3""23dni";

	{
		DDRB |= 1<<PB2; //PIEZO: resistor value should be corrected
		PORTB &= ~(1<<PB2);
		_delay_ms(200);
		PORTB |= (1<<PB2);
	}

	sei();
	DB_Initialize();

	Drv_TimerInit();
	UI_displayInit();

	Peripherials_Initialize();
	struct sm_kbd* smKbd = SM_KbdInit();
	RTC_Initialize();
	SM_CtrlFeedsInit();

	UART_Initialize();
	
	SM_TempInit();
	SM_CtrlTempInit();
	SM_CtrlLightsInit();

	UI_customCharInit();
	UI_ScreenMgrInit();

	UI_ScreenMainRegisterScreen();
	UI_ScreenMenuRegisterScreen();
	UI_ScreenMenuSetDateRegisterScreen();
	UI_ScreenMenuSetTimeRegisterScreen();
	LCD_GoTo(0,3);

	UI_ScreenMgrSwitch(UI_SCR_MGR_MAIN);

	while (1) {
		wdt_reset();
		UI_ScreenMgrRun();
		SM_TempRun();
		SM_KbdRun(smKbd);
		SM_CtrlTempTick();
		SM_CtrlLightsTick();
		RTC_Run();
		Drv_TimerRun();
		SM_CtrlFeedsTick();
		UI_displayTick();
		UI_customCharTick();
	}

	return 0;
}
