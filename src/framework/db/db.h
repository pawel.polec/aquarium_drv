/*
 * db.h
 *
 *  Created on: 10 wrz 2017
 *      Author: Pawe�
 */

#ifndef FRAMEWORK_DB_DB_H_
#define FRAMEWORK_DB_DB_H_

#include "framework/typedefs.h"
#include "framework/error.h"

enum DBDataField {
	DB_VERSION,
	DB_FIELD_ID_LIGHT_ON,
	DB_FIELD_ID_LIGHT_OFF,
	DB_FIELD_ID_TMP_SET,
	DB_FIELD_ID_TMP_HIST,
	DB_FIELD_ID_FEED_TIME,
	DB_FIELD_ID_TIMESTAMP,

	DB_FIELDS_COUNT,
	DB_FIELD_ID_NULL = 0xFF
};

typedef struct db DBInstance;

Error DB_Initialize ();
Error DB_Load (DBInstance **db);
Error DB_GetDB (DBInstance **db);

Error DB_GetEntry (DBInstance *db, enum DBDataField ID, uint8_t *data, uint8_t size);
Error DB_PutEntry (DBInstance *db, enum DBDataField ID, uint8_t *data, uint8_t size);
Error DB_FactoryReset (DBInstance *db);

Error DB_OnChangeRegister (DBInstance *db, enum DBDataField ID, void (*onChange) ());
Error DB_OnChangeUnregister (DBInstance *db, enum DBDataField ID, void (*onChange) ());

#endif /* FRAMEWORK_DB_DB_H_ */
