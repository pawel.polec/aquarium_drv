//-------------------------------------------------------------------------------------------------
// Wywietlacz alfanumeryczny ze sterownikiem HD44780
// Sterowanie w trybie 4-bitowym bez odczytu flagi zajêtoci
// z dowolnym przypisaniem sygna³ów steruj¹cych
// Plik : HD44780.c	
// Mikrokontroler : Atmel AVR
// Kompilator : avr-gcc
// Autor : Rados³aw Kwiecieñ
// ród³o : http://radzio.dxp.pl/hd44780/
// Data : 24.03.2007
//-------------------------------------------------------------------------------------------------

#include "HD44780.h"
#include "avr/pgmspace.h"

#include "ui/view/elements/custom_char.h"

volatile unsigned char LCD_LOCKED = 0;

//-------------------------------------------------------------------------------------------------
//
// Funkcja wystawiaj¹ca pó³bajt na magistralê danych
//
//-------------------------------------------------------------------------------------------------
void _LCD_OutNibble(unsigned char nibbleToWrite)
{
if(nibbleToWrite & 0x01)
	LCD_DB4_PORT |= LCD_DB4;
else
	LCD_DB4_PORT  &= ~LCD_DB4;

if(nibbleToWrite & 0x02)
	LCD_DB5_PORT |= LCD_DB5;
else
	LCD_DB5_PORT  &= ~LCD_DB5;

if(nibbleToWrite & 0x04)
	LCD_DB6_PORT |= LCD_DB6;
else
	LCD_DB6_PORT  &= ~LCD_DB6;

if(nibbleToWrite & 0x08)
	LCD_DB7_PORT |= LCD_DB7;
else
	LCD_DB7_PORT  &= ~LCD_DB7;
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu bajtu do wywietacza (bez rozró¿nienia instrukcja/dane).
//
//-------------------------------------------------------------------------------------------------
void _LCD_Write(unsigned char dataToWrite)
{
LCD_E_PORT |= LCD_E;
_LCD_OutNibble(dataToWrite >> 4);
LCD_E_PORT &= ~LCD_E;
LCD_E_PORT |= LCD_E;
_LCD_OutNibble(dataToWrite);
LCD_E_PORT &= ~LCD_E;
_delay_us(50);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu rozkazu do wywietlacza
//
//-------------------------------------------------------------------------------------------------
void LCD_WriteCommand(unsigned char commandToWrite)
{
LCD_RS_PORT &= ~LCD_RS;
_LCD_Write(commandToWrite);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu danych do pamiêci wywietlacza
//
//-------------------------------------------------------------------------------------------------
void LCD_WriteData(unsigned char dataToWrite)
{
LCD_RS_PORT |= LCD_RS;
_LCD_Write(dataToWrite);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja wyszukująca polskie znaki diakratyczne
//
//-------------------------------------------------------------------------------------------------

//
// PL_Chars (CP 1250):
// DEC  HEX  CH
// 140 0x8c   Ś
// 143 0x8f   Ź
// 156 0x9c   ś
// 159 0x9f   ź
// 163 0xa3   Ł
// 165 0xa5   Ą
// 175 0xaf   Ż
// 179 0xb3   ł
// 185 0xb9   ą
// 191 0xbf   ż
// 198 0xc6   Ć
// 202 0xca   Ę
// 209 0xd1   Ń
// 211 0xd3   Ó
// 230 0xe6   ć
// 234 0xea   ę
// 241 0xf1   ń
// 243 0xf3   ó
// 

static char _validateChar(char c) {
	switch ((unsigned char) c) {
		case 140: //Ś
		{
			uint8_t idx=UI_customCharGetCharIdx(CUSTOM_CHAR_CAP_S_WITH_ACUTE_ACCENT);
			if (idx > CUSTOMCHAR_SPACE) return 'S';
			return (char) idx;
		}
		break;

		case 156: //ś
		{
			uint8_t idx=UI_customCharGetCharIdx(CUSTOM_CHAR_S_WITH_ACUTE_ACCENT);
			if (idx > CUSTOMCHAR_SPACE) return 's';
			return (char) idx;
		}
		break;

		case 202: //Ę
		{
			uint8_t idx=UI_customCharGetCharIdx(CUSTOM_CHAR_CAP_E_WITH_OGONEK);
			if (idx > CUSTOMCHAR_SPACE) return 'E';
			return (char) idx;
		}

		case 234: //ę
		{
			uint8_t idx=UI_customCharGetCharIdx(CUSTOM_CHAR_E_WITH_OGONEK);
			if (idx > CUSTOMCHAR_SPACE) return 'e';
			return (char) idx;
		}

		case 209: //Ń
		{
			uint8_t idx=UI_customCharGetCharIdx(CUSTOM_CHAR_CAP_N_WITH_ACUTE_ACCENT);
			if (idx > CUSTOMCHAR_SPACE) return 'N';
			return (char) idx;
		}

		case 241: //ń
		{
			uint8_t idx=UI_customCharGetCharIdx(CUSTOM_CHAR_N_WITH_ACUTE_ACCENT);
			if (idx > CUSTOMCHAR_SPACE) return 'n';
			return (char) idx;
		}

		case 163: //Ł
		{
			uint8_t idx=UI_customCharGetCharIdx(CUSTOM_CHAR_CAP_L_WITH_STROKE);
			if (idx > CUSTOMCHAR_SPACE) return 'L';
			return (char) idx;
		}

		case 179: //ł
		{
			uint8_t idx=UI_customCharGetCharIdx(CUSTOM_CHAR_L_WITH_STROKE);
			if (idx > CUSTOMCHAR_SPACE) return 'l';
			return (char) idx;
		}
		break;

		default:
			return c;
	}
}

//-------------------------------------------------------------------------------------------------
//
// Funkcja wywietlenia napisu na wyswietlaczu.
//
//-------------------------------------------------------------------------------------------------
void LCD_WriteText(const char * text)
{
	while (LCD_LOCKED);

	LCD_LOCKED = 1;
while(*text)
  LCD_WriteData(_validateChar(*text++));

LCD_LOCKED = 0;
}

//-------------------------------------------------------------------------------------------------
//
// Odpowiednik LCD_WriteText pracuj¹cy na pamiêci FLASH
//
//-------------------------------------------------------------------------------------------------
void LCD_WriteTextP(const char * text)
{
	while (LCD_LOCKED);
	LCD_LOCKED = 1;

	register char c;

	while((c = pgm_read_byte(text++)))
		LCD_WriteData(_validateChar(c));

	LCD_LOCKED = 0;
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja ustawienia wspó³rzêdnych ekranowych
//
//-------------------------------------------------------------------------------------------------
void LCD_GoTo(unsigned char x, unsigned char y)
{
	while (LCD_LOCKED);

	LCD_LOCKED = 1;
//LCD_WriteCommand(HD44780_DDRAM_SET | (x + (0x40 * y)));
	LCD_WriteCommand(HD44780_DDRAM_SET | ((x + ((0x40 * (y & 0b01)) + (quantiti_of_characters * ((y & 0b10)>>1))))));	//dla wyswietlacza 4x20 znakow

	LCD_LOCKED = 0;
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja czyszczenia ekranu wywietlacza.
//
//-------------------------------------------------------------------------------------------------
void LCD_Clear(void)
{
LCD_WriteCommand(HD44780_CLEAR);
_delay_ms(2);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja przywrócenia pocz¹tkowych wspó³rzêdnych wywietlacza.
//
//-------------------------------------------------------------------------------------------------
void LCD_Home(void)
{
LCD_WriteCommand(HD44780_HOME);
_delay_ms(2);
}
//-------------------------------------------------------------------------------------------------
//
// Procedura inicjalizacji kontrolera HD44780.
//
//-------------------------------------------------------------------------------------------------
void LCD_Initalize(void)
{
unsigned char i;
LCD_DB4_DIR |= LCD_DB4; // Konfiguracja kierunku pracy wyprowadzeñ
LCD_DB5_DIR |= LCD_DB5; //
LCD_DB6_DIR |= LCD_DB6; //
LCD_DB7_DIR |= LCD_DB7; //
LCD_E_DIR 	|= LCD_E;   //
LCD_RS_DIR 	|= LCD_RS;  //
_delay_ms(15); // oczekiwanie na ustalibizowanie siê napiecia zasilajacego
LCD_RS_PORT &= ~LCD_RS; // wyzerowanie linii RS
LCD_E_PORT &= ~LCD_E;  // wyzerowanie linii E

for(i = 0; i < 3; i++) // trzykrotne powtórzenie bloku instrukcji
  {
  LCD_E_PORT |= LCD_E; //  E = 1
  _LCD_OutNibble(0x03); // tryb 8-bitowy
  LCD_E_PORT &= ~LCD_E; // E = 0
  _delay_ms(5); // czekaj 5ms
  }

LCD_E_PORT |= LCD_E; // E = 1
_LCD_OutNibble(0x02); // tryb 4-bitowy
LCD_E_PORT &= ~LCD_E; // E = 0

_delay_ms(1); // czekaj 1ms 
LCD_WriteCommand(HD44780_FUNCTION_SET | HD44780_FONT5x7 | HD44780_TWO_LINE | HD44780_4_BIT); // interfejs 4-bity, 2-linie, znak 5x7
LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_OFF); // wy³¹czenie wyswietlacza
LCD_WriteCommand(HD44780_CLEAR); // czyszczenie zawartosæi pamieci DDRAM
_delay_ms(2);
LCD_WriteCommand(HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT);// inkrementaja adresu i przesuwanie kursora
LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_OFF | HD44780_CURSOR_NOBLINK); // w³¹cz LCD, bez kursora i mrugania
}

//-------------------------------------------------------------------------------------------------
//
// Koniec pliku HD44780.c
//
//-------------------------------------------------------------------------------------------------
