/*
 * rtc.c
 *
 *  Created on: 3 sie 2016
 *      Author: Pawe�
 */

#include "rtc.h"
#include "avr/io.h"
#include "avr/interrupt.h"
#include "avr/pgmspace.h"
#include "drv/port/port.h"
#include "drv/LCD/HD44780.h"
#include "framework/db/db.h"

#define RTC_TIME_CHANGE_CBF (3)
#define RTC_ALARM_CBF (3)

static volatile time_t timestamp;
static volatile bool secChanged;
__flash const char * __flash const daysName[] = {
		(__flash const char[]){"Nd"},
		(__flash const char[]){"Pn"},
		(__flash const char[]){"Wt"},
		(__flash const char[]){"\x8Cr"},
		(__flash const char[]){"Cz"},
		(__flash const char[]){"Pt"},
		(__flash const char[]){"So"}
};

static struct rtc {
	onSecChangedCbf onSecCfb[RTC_TIME_CHANGE_CBF];
	onSecChangedCbf onTimeChangeCfb[RTC_TIME_CHANGE_CBF];
	struct RTC_alarm alarms[RTC_ALARM_CBF];
	bool initialized;
} instance = {0};

static void _notifySecChange() {
	uint8_t i;
	for (i=0; i<RTC_TIME_CHANGE_CBF; ++i) {
		if (instance.onSecCfb[i] != NULL){
			instance.onSecCfb[i]();
		}
	}
}

static void _notifyTimeChange() {
	uint8_t i;
	for (i=0; i<RTC_TIME_CHANGE_CBF; ++i) {
		if (instance.onTimeChangeCfb[i] != NULL){
			instance.onTimeChangeCfb[i]();
		}
	}
}


static void _checkAlarms () {
	uint8_t i;
	for (i=0; i<RTC_ALARM_CBF; ++i) {
		if (timestamp%ONE_DAY == instance.alarms[i].time) {
			if (instance.alarms[i].alarmCbf != NULL){
				instance.alarms[i].alarmCbf();
			}
		}
	}
}

ISR(TIMER2_COMP_vect, ISR_NOBLOCK) {
	++timestamp;
	secChanged = true;
}

static void _copyTm(struct tm * src, struct tm * dst) {
	*dst = *src;
	//dst->tm_zone = NULL;
}

Error RTC_StoreTime () {
	DBInstance *db;
    DB_GetDB(&db);
	time_t tmp = timestamp;

    return DB_PutEntry(db, DB_FIELD_ID_TIMESTAMP, (uint8_t*) &tmp, sizeof(tmp));
}

Error RTC_Initialize () {
	Error ret = NO_ERROR;

	DBInstance *db;
    DB_GetDB(&db);

    if (ERROR_NOT_FOUND ==  DB_GetEntry(db, DB_FIELD_ID_TIMESTAMP, (uint8_t*) &timestamp, sizeof(timestamp))) {
        timestamp = 0;
    }

	TCCR2 = 0; //stop timer
	instance.initialized = false;

	//AS2  = 1   - asynchronous mode.
	ASSR |= 1<<AS2;


	//OCR2 = 128 - 1 sec on timer compare
	OCR2 = 127;

	//OCIE2 = 1 - enable output compare interrupt
	TIMSK |= 1<<OCIE2;

	//WGM21:0 - 10 - CTC (clear on time compare);
	//COM21:0 - 00 - Normal port mode - disconnected from timer;
	//FOC2    - 0  - no force...
	//CS22:0   -110 - Prescaler = f/256

	TCCR2 |= ( 1<<WGM21 ) | ( 1<<CS22 ) | ( 1<<CS21 );
//	TCCR2 |= ( 1<<WGM21 ) | ( 1<<CS20 );
	instance.initialized = true;

	return ret;
}

Error RTC_Run () {
	if (!secChanged) return NO_ERROR;
	_notifySecChange();
	_checkAlarms ();
	return NO_ERROR;
}

//TODO: add enum to decide if we want to get date, time or both
Error RTC_GetTime (struct tm * timeinfo) {
	Error ret = NO_ERROR;

	if (timeinfo != NULL && instance.initialized) {
		struct tm * tmi = gmtime((time_t*)&timestamp);
		_copyTm(tmi, timeinfo);
	} else if (!instance.initialized){
		ret = ERROR_UNINITIALIZED;
	} else {
		//TODO: Add cexceptions to project!
		ret = ERROR_INVALID_PARAMETER;
	}

	return ret;
}




//TODO: add enum to decide if we want to get date, time or both
Error RTC_SetTime (struct tm * timeinfo) {
	Error ret = NO_ERROR;

	if (timeinfo != NULL && instance.initialized) {
		time_t tempts = mktime (timeinfo);
		if (tempts < 0) {
			ret = ERROR_INVALID_OPERATION;
		} else {
			timestamp = tempts;
		}
	} else if (!instance.initialized) {
		ret = ERROR_UNINITIALIZED;
	} else {
		//TODO: Add cexceptions to project!
		ret = ERROR_INVALID_PARAMETER;
	}

	_notifyTimeChange();

	return ret;
}

__flash const char* RTC_GetDayName(uint8_t dayOfWeek) {
	return daysName[dayOfWeek%7];
}

uint8_t RTC_GetDayOfWeek() {
	struct tm * timeinfo = localtime((time_t*)&timestamp);
	return (uint8_t) timeinfo->tm_wday;
}

Error RTC_RegisterOnSecCbf (onSecChangedCbf cbf) {
	uint8_t i;
    for (i=0; i<RTC_TIME_CHANGE_CBF; ++i) {
        if (instance.onSecCfb[i] == NULL){
            instance.onSecCfb[i] = cbf;
            return NO_ERROR;
        }
    }
    return ERROR_INVALID_PARAMETER;
}

Error RTC_UnegisterOnSecCbf (onSecChangedCbf cbf) {
    uint8_t i;
    for (i=0; i<RTC_TIME_CHANGE_CBF; ++i) {
        if (instance.onSecCfb[i] == cbf){
            instance.onSecCfb[i] = NULL;
            return NO_ERROR;
        }
    }
    return ERROR_INVALID_PARAMETER;
}

Error RTC_RegisterOnTimeChangeCbf (onSecChangedCbf cbf) {
	uint8_t i;
    for (i=0; i<RTC_TIME_CHANGE_CBF; ++i) {
        if (instance.onTimeChangeCfb[i] == NULL){
            instance.onTimeChangeCfb[i] = cbf;
            return NO_ERROR;
        }
    }
    return ERROR_INVALID_PARAMETER;
}

Error RTC_UnregisterOnTimeChangeCbf (onSecChangedCbf cbf) {
    uint8_t i;
    for (i=0; i<RTC_TIME_CHANGE_CBF; ++i) {
        if (instance.onTimeChangeCfb[i] == cbf){
            instance.onTimeChangeCfb[i] = NULL;
            return NO_ERROR;
        }
    }
    return ERROR_INVALID_PARAMETER;
}

Error RTC_RegisterAlarm (struct RTC_alarm alarm) {
	uint8_t i;
    for (i=0; i<RTC_ALARM_CBF; ++i) {
        if (instance.alarms[i].alarmCbf == NULL){
            instance.alarms[i] = alarm;
            return NO_ERROR;
        }
    }
    return ERROR_INVALID_PARAMETER;
}

Error RTC_UnregisterAlarm (struct RTC_alarm alarm)  {
	uint8_t i;
    for (i=0; i<RTC_ALARM_CBF; ++i) {
        if (instance.alarms[i].alarmCbf == alarm.alarmCbf && instance.alarms[i].time == alarm.time ){
            instance.alarms[i].alarmCbf = NULL;
            instance.alarms[i].time = 0;
            return NO_ERROR;
        }
    }
    return ERROR_INVALID_PARAMETER;
}
