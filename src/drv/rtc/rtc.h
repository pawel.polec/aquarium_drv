/*
 * rtc.h
 *
 *  Created on: 3 sie 2016
 *      Author: Paweł Połec
 */

#ifndef DRV_RTC_RTC_H_
#define DRV_RTC_RTC_H_

#include "framework/error.h"
#include "framework/typedefs.h"

#include <time.h>

#define RTC_BASE_YEAR 2000

typedef void (*onSecChangedCbf) ();
typedef void (*onAlarm) ();

struct RTC_alarm {
	time_t time;
	onAlarm alarmCbf;
};

Error RTC_Initialize ();
Error RTC_GetTime (struct tm * timeinfo);
Error RTC_SetTime (struct tm * timeinfo);
Error RTC_RegisterOnSecCbf (onSecChangedCbf cbf);
Error RTC_UnegisterOnSecCbf (onSecChangedCbf cbf);
Error RTC_RegisterOnTimeChangeCbf (onSecChangedCbf cbf);
Error RTC_UnregisterOnTimeChangeCbf (onSecChangedCbf cbf);
Error RTC_RegisterAlarm (struct RTC_alarm alarm);
Error RTC_UnregisterAlarm (struct RTC_alarm alarm);
Error RTC_Run ();
Error RTC_StoreTime ();


__flash const char* RTC_GetDayName(uint8_t dayOfWeek);
uint8_t RTC_GetDayOfWeek();

#endif /* DRV_RTC_RTC_H_ */
