/*
 * timer.h
 *
 *  Created on: 06 Sep 2021
 *      Author: Paweł Połeć
 */

#ifndef DRV_TIMER_H_
#define DRV_TIMER_H_

#include "framework/error.h"
#include "framework/typedefs.h"

typedef void (*timerCbf)();

enum timer_type {
    DRV_TIMER_ONE_TIME,
    DRV_TIMER_INTERVAL,
};

struct drv_timer {
    enum timer_type type;
    timerCbf cbf;
    uint16_t ticks10ms;
};

Error Drv_TimerInit();
Error Drv_TimerRegister(const struct drv_timer *);
Error Drv_TimerUnregister(const struct drv_timer *);
Error Drv_TimerRun();

/*

INIT: set interrupt for 0.01s

Timer types: ONE_TIME, INTERVAL



*/

#endif /* DRV_TIMER_H_ */