
#include "timer.h"
#include "avr/io.h"
#include "avr/interrupt.h"

#define DRV_TIMERS_COUNT (10)

static struct drv_timer_instance {
    struct drv_timer timers[DRV_TIMERS_COUNT];
    uint16_t timersValues[DRV_TIMERS_COUNT];
    bool initialised;
} instance = {0};

volatile uint8_t sysTicks=0;

ISR(TIMER0_COMP_vect, ISR_NOBLOCK) {
    ++sysTicks;
}

static void _initISR() {

	TCCR0 = 0; //stop timer
	instance.initialised = false;


	//OCR0 = 77 ~10 ms on timer compare
	OCR0 = 77;

	//OCIE0 = 1 - enable output compare interrupt
	TIMSK |= 1<<OCIE0;

	//WGM01:0 = 2 - CTC (clear on time compare);
	//COM01:0 - 00 - Normal port mode - disconnected from timer;
	//FOC0    - 0  - no force...
	//CS22:0   -101 - Prescaler = f/1024

	TCCR0 |= ( 1<<WGM01 ) | ( 1<<CS02 ) | ( 1<<CS00 );
}

Error Drv_TimerInit() {
    _initISR();

    instance.initialised = true;
    return NO_ERROR;
}

Error Drv_TimerRegister(const struct drv_timer *timer) {
    uint8_t i;
    if (!timer) return ERROR_INVALID_PARAMETER;
    if (!instance.initialised) return ERROR_UNINITIALIZED;

    for (i=0; i<DRV_TIMERS_COUNT; ++i) {
        if (instance.timers[i].cbf == NULL) {
            instance.timers[i].cbf = timer->cbf;
            instance.timers[i].type = timer->type;
            instance.timers[i].ticks10ms = timer->ticks10ms;
            instance.timersValues[i] = timer->ticks10ms;
            return NO_ERROR;
        }
    }

    return ERROR_INSUFFICIENT_RESOURCES;
}

Error Drv_TimerUnregister(const struct drv_timer *timer) {
    uint8_t i;
    
    if (!timer) return ERROR_INVALID_PARAMETER;
    if (!instance.initialised) return ERROR_UNINITIALIZED;

    for (i=0; i<DRV_TIMERS_COUNT; ++i) {
        if (instance.timers[i].cbf == timer->cbf) {
            instance.timers[i].cbf = NULL;
            return NO_ERROR;
        }
    }
    return ERROR_NOT_FOUND;
}

Error Drv_TimerRun() {
    uint8_t i;

    if (sysTicks==0) return NO_ERROR;

    for (i=0; i<DRV_TIMERS_COUNT; ++i) {
        if (instance.timers[i].cbf != NULL) {
            if (instance.timersValues[i] > sysTicks) {
                instance.timersValues[i]-=sysTicks;
            } else {
                if (instance.timers[i].type == DRV_TIMER_INTERVAL) {
                    instance.timersValues[i] = instance.timers[i].ticks10ms - (sysTicks - instance.timersValues[i]);
                    instance.timers[i].cbf();
                } else {
                    instance.timers[i].cbf();
                    instance.timers[i].cbf = NULL;
                }
            }
        }
    }

    sysTicks = 0;

    return NO_ERROR;
}
